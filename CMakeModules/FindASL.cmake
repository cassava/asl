# Find ASL library.
#
# Set ASL_ROOT to provide a hint as to where the files are found.
# It is expected that the structure looks something _like_ this,
# although the include/ and lib/ directories are optional.
#
#    include/
#       asl/
#           asl.h
#           ...
#    lib/
#       libasl.a
#
# Once done this will define:
#    ASL_FOUND         - System has ASL
#    ASL_INCLUDE_DIRS  - The ASL include directories
#    ASL_LIBRARIES     - The libraries needed to use ASL

find_path(ASL_INCLUDE_DIR asl/asl.h
    HINTS
    ${ASL_ROOT}
    PATH_SUFFIXES include
)

find_library(ASL_LIBRARY NAMES asl
    HINTS
    ${ASL_ROOT}
    PATH_SUFFIXES lib bin
)

find_library(ASL_LIBRARY_DEBUG NAMES asld
    HINTS
    ${ASL_ROOT}
    PATH_SUFFIXES lib bin
)
if(NOT ASL_LIBRARY_DEBUG)
    message("Could not find debug ASL library.")
    if(UNIX)
        message("Setting debug ASL library to same as optimized.")
        set(ASL_LIBRARY_DEBUG ${ASL_LIBRARY})
    endif()
endif()

set(ASL_LIBRARIES optimized ${ASL_LIBRARY} debug ${ASL_LIBRARY_DEBUG})
set(ASL_INCLUDE_DIRS ${ASL_INCLUDE_DIR})

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(ASL DEFAULT_MSG ASL_LIBRARY ASL_INCLUDE_DIR)

mark_as_advanced(ASL_INCLUDE_DIR ASL_LIBRARY ASL_LIBRARY_DEBUG)
