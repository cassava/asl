Release Information
===================

## Version 0.20 (17 February 2015)
This release makes integration with the CSL project much easier, among fixing
several bugs. The major change involves the interface of the Algorithm class.
Instead of taking full control of the OSG Viewer, it is possible to use hooks:

    osgGA::NodeTrackerManipulator* router_initialize(osgViewer::Viewer& viewer, osg::Group* root, const straph::Route* route);
    void router_startup();
    bool router_next(osgViewer::Viewer& viewer, osg::Group* root);
    int router_shutdown();

For more information on how to use it, see how these functions are used
in the `route_network` function.

Other minor changes and bugfixes include:

  * Change: Using straph version 1.3 now, many routes may be invalid now.
  * Bugfix: Global Color variables did not work due to C++ implementation
    quirk.
  * Bugfix: Unhandled exception during font loading.

## Version 0.19 (5 January 2015)
Happy new year! And with that, I release the first improvement to tourable:
Linear interpolation is applied to camera movement (following the pointer);
this is not a preprocessing step. This means you are free to stop the pointer,
speed up, etc.

  * New: Camera movement uses linear interpolation (aka: lerping).
  * New: Option `camera_lerp` can be set in `[voyager]` to adjust the lerp.

## Version 0.18 (19 December 2014)
We revert the behavior of version 0.17 temporarily by introducing
a compile time option to control this behavior.

  * Changed: The `insignificance_threshold` option has moved from the
    `[force_directed]` to the `[static]` section. This has been
    updated in `example_config.ini`.
  * Changed: The calculated visible overlap now discards labels that
    are too small, as given by `insignificance_threshold`.
  * Changed: I refactored the API somewhat, to support some of the features
    for this release. In particular, `util/screenspace.h` changed.
    This change should actually result in higher efficiency.
  * New: Statistics are calculated for leader heights in the screen space.
  * New: CMake option `ENABLE_EXPERIMENTAL_LABEL_ENTRY` available.
    When on, this causes labels to slide in from the top (given by the
    maximum current leader height). It also changes the way temperatures are
    adjusted.
    calculated.

## Version 0.17 (18 December 2014)
This release brings a welcome change in the algorithm (yikes!).
Edit: Because of a mistake, it is not possible to checkout this version. :-(
But you can enable the behavior in the next version.

  * Changed: labels that are inserted during the course of the route are
    inserted with maximum height among all active labels. This helps prevent
    an inverse staircase from appearing: label 1 would be above 2, which was
    above 3, and so on. This would happen because labels would be inserted
    with a low height, appearing beneath the already elevated labels.
    This is not only ugly, but is hard to read.
  * Changed: to support the above change, the temperature of labels is not
    reduced when they go from force 0 to a negative force.

In total these two changes allow labels to appear high, but quickly drop down
to their desired height.

## Version 0.16 (18 December 2014)
This release changes one compile-time definition and adds a few compile-time
options.

  * Changed: CMake option `USE_IMAGE_DEBUGGING` has been renamed to
    `ENABLE_IMAGE_DEBUGGING`, as has the definition in the code.
  * New: CMake option `ENABLE_ANTIALIASING` available. This slows down the
    frame rates on my computer to around 30 fps. Maybe you will have better
    luck.
  * New: CMake option `ENABLE_LIGHTING` available. This is by default off,
    which means that the streets, leaders, and pointer now are not lighted,
    which means that their color displays fully.

I'd like to bring up one problem with the code: the dates in the files are
not necessarily true. It is better to use Git to determine when files were
last changed.

## Version 0.15 (17 December 2014)
This release requires straph version >= 1.1 to compile.

  * New: Multiple routes can now be specified.
  * New: On commandline, argument -p1 can be specified to print the list
    of route points. The indices of all the major junctions are printed,
    which can then be put into a configuration file, or on the command
    line.
  * Bugfix: Statistics module tried to write empty files. Now it correctly
    recognizes this as an error.

With multiple route points, streets may be reused. This may lead to
multiple labels defined over each other. This will need to be fixed
in the future.
