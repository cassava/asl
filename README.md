# ASL and Tourable

The Active Street Labeling (ASL) library contains multiple algorithms to
display and follow Straph routes.

This library has been extracted out of the previous streetlabel project in
order to separate out the relevant components that relate to the actual
labeling process. Two algorithms can do roughly the same thing (follow a route
and label streets) yet do it in very different ways. This library gives a
lot of leeway to the algorithms by defining only an interface and letting
them sort out how to comply. To ease the job, various routines such as drawing
an actual label or defining colors have been brought into a `util` package.

There are many algorithms that you can choose from:

 * `disable` quits before showing anything, or possibly only shows the street
   network (can't remember).
 * `base` simply draws the street graph and colors the route red, and provides
   movement along it via the voyager.
 * `static` draws the street labels in static positions.
 * Force-directed algorithms cause labels to avoid overlapping. There are
   at the moment four such algorithms, the first three are deprecated:
    - `linear_fd` uses a simple linear force growth from Fbase to Flimit.
    - `quadratic_fd` uses a quadratic/exponential growth from Fbase to Flimit.
    - `exponential_fd` same as above, with some changes. I forgot.
    - `spring_matching_fd` is the most advanced force-directed algorithm.

It is possible to inherit from these algorithms and define new algorithms
based off them. In fact, `static` inherits from `base`, and all the
force-directed algorithms inherit from a single force-directed core.

## Usage
The ASL package generates the `asl` library as well as the `tourable`
executable. See the `tourable --help` for more information on the usage.

There is an examle ini file for tourable which is also well documented.

### VSYNC
On some machines, the frame rate is limited to the refresh rate of the monitor;
this normally makes a lot of sense, but if we want to “benchmark” our program,
it can get in the way. This behaviour is enforced by the graphics card driver,
and disabling it is therefore dependent on the graphics card, driver, and
operating system. In Linux for the Intel graphics card, you can run tourable
like this:

    vblank_mode=0 tourable test_alien.ini

For other systems, you are going to have to look yourself how it is done.

## Logging
Like Straph, ASL uses [log4cxx](http://logging.apache.org/log4cxx/) as its
logging framework, which means that you can specify in your own programs
exactly what and how it logs.

The namespaces contained in ASL are

  * `straph` for the straph library
  * `asl.*` for various utilities, such as
      - `asl.keh` for keyboard event handling
      - `asl.draw` for drawing of objects
          - `asl.draw.street` for drawing of streets
          - `asl.draw.pointer` for drawing the pointer
          - `asl.draw.label` for drawing of labels
      - `asl.voyager` for voyager information
          - `asl.voyager.controller` for voyager control
      - `asl.debugger` for frame debugging information
      - `asl.svg` for SVG file writing (part of debugging, actually)
      - `asl.router` for augmented route movement
      - `asl.statistics` for statistics output
  * `asl.algorithm.*` for the various router algorithms, such as
      - `asl.algorithm.base` for the basic algorithm and
      - `asl.algorithm.static` for static labeling algorithm
      - `asl.algorithm.force-directed` for force-directed algorithm
  * `tourable` for the tourable program

See the log4cxx website for more information on this.

## Compilation
When linking this library `asl` in, you (probably) need to link in the
following libraries as well:

 * `straph` (>= version 1.1)
 * `log4cxx`
 * `boost_program_options`
 * `boost_system`
 * `boost_thread`
 * `boost_filesystem`
 * `boost_graph` (maybe)
 * `boost_chrono` (maybe)
 * `osg`
 * `osgDB`
 * `osgUtil`
 * `osgGA`
 * `osgViewer`
 * `osgText`
 * `OpenThreads`
 * `freetype`
 * `gl` (OpenGL libraries)

### Compilation in Windows
If you are planning on compiling this yourself, you will need at least 10 GB of
free space on your harddrive (sad, I know.)

In this guide, I am going to assume that `Development` is the folder where you
have your sources.

First, let's install the software that we need:

 1. Install [Git](git-scm.com)
 2. Install [CMake](www.cmake.org)
 3. Install [Visual Studio 2013](http://www.microsoft.com/de-de/download/details.aspx?id=40787)
    I would love to say that Visual Studio Express 2013 works,
    but sadly it doesn't, due to a bug in Visual Studio Express.
    Maybe it has been fixed, but I don't know.

Now we need to install all the dependencies of the project:

 4. Install the [Boost](www.boost.org) libraries. You should be able to
    download the precompiled binaries for this.
 5. Compile the [OpenSceneGraph](www.openscenegraph.org) libraries. This is
    pretty straightforward, but the compilation itself is going to take a long
    time (a few hours). Consider downloading the files if you in any way can.
 6. Install the [log4cxx] library.
    This is [slightly](http://logging.apache.org/log4cxx/building/vstudio.html)
    [more](http://codetato.wordpress.com/2012/11/15/building-log4cxx-with-visual-studio-2012/)
    [involved](http://www.codeproject.com/Tips/492585/How-to-build-log-cxx-with-Visual-Studio).
    Sorry about that.

Or, if you want to save yourself a lot of time and hassle, download the
dependencies directly from the ASL site (for Visual Studio 2013).
