/*
 * \file asl.cc
 *
 * \author Ben Morgan <benjamin.morgan@stud-mail.uni-wuerzburg.de>
 * \date   6. February 2015
 */

#include "asl.h"
#include "statistics.h"
#include "util/draw.h"
#include "util/timer.h"
#include "util/filewriter.h"

#include <string>
using namespace std;

#include <osg/ref_ptr>
#include <osg/Vec3d>
#include <osgGA/NodeTrackerManipulator>
#include <osgUtil/Optimizer>
#include <osgViewer/Viewer>
#include <osgViewer/ViewerEventHandlers>
using namespace osg;

#include <log4cxx/logger.h>

// ALGO_NS is the configuration namespace for this class.
#define ALGO_NS "rendering."

namespace asl {

namespace {

log4cxx::LoggerPtr logger = log4cxx::Logger::getLogger("asl.algorithm.base");

/**
 * Optimize the OSG scene so that it runs faster.
 *
 * TODO: Find out if there are more things that you can do here.
 */
void optimize_scene(osg::Group* root)
{
    LOG4CXX_DEBUG(logger, "Optimizing scene graph...");

    osgUtil::Optimizer optimizer;
    optimizer.optimize(root);
}

} // anonymous namespace

void Algorithm::options(boost::program_options::options_description& opt)
{
    namespace po = boost::program_options;

    opt.add_options()
        (ALGO_NS "street_width"              , po::value<double>()->default_value(2.0)                               , "Line width, in pixels, of normal streets.")
        (ALGO_NS "street_color"              , po::value<string>()->default_value(tango2html(TangoColor::Aluminium5)), "Color of normal streets.")
        (ALGO_NS "background_color"          , po::value<string>()->default_value(tango2html(TangoColor::White))     , "Color of background sky and ground.")
        (ALGO_NS "route_width"               , po::value<double>()->default_value(4.0)                               , "Line width, in pixels, of the streets belonging to the route.")
        (ALGO_NS "route_color"               , po::value<string>()->default_value(tango2html(TangoColor::Scarlet3))  , "Color of the active route.")

        (ALGO_NS "pointer_size"              , po::value<double>()->default_value(6.0)                               , "Width, in osg units, of the pointer.")
        (ALGO_NS "pointer_color"             , po::value<string>()->default_value(tango2html(TangoColor::Chameleon3)), "Color of the pointer body.")
        (ALGO_NS "pointer_border_color"      , po::value<string>()->default_value(tango2html(TangoColor::Aluminium6)), "Color of the pointer border.")
        (ALGO_NS "pointer_elevation"         , po::value<double>()->default_value(1/30.0)                            , "Elevation of the pointer over the route.")

        (ALGO_NS "label_font_path"           , po::value<string>()                                                   , "Path to font used for labeling.")
        (ALGO_NS "label_font_size"           , po::value<double>()->default_value(12.0)                              , "Point size of the label font (may need to be small).")
        (ALGO_NS "label_font_color"          , po::value<string>()->default_value(tango2html(TangoColor::Black))     , "Color of the label font.")
        (ALGO_NS "label_draw_box"            , po::value<bool>()->default_value(true)                                , "Whether to draw the label with a bounding box and leader.")
        (ALGO_NS "label_fill_box"            , po::value<bool>()->default_value(false)                               , "Whether to fill the label bounding box (instead of outlining it).")
        (ALGO_NS "label_box_background_color", po::value<string>()->default_value(tango2html(TangoColor::Aluminium1)), "Color of the label box background (only when filling).")
        (ALGO_NS "label_box_border_color"    , po::value<string>()->default_value(tango2html(TangoColor::Aluminium4)), "Color of the label box border and leader (only when not filling).")
        (ALGO_NS "label_box_margin"          , po::value<double>()->default_value(5.0)                               , "Internal padding, in osg units?, of the label bounding box.")

        (ALGO_NS "camera_height"             , po::value<double>()->default_value(25.0)                              , "Default camera height.")
        (ALGO_NS "camera_look_ahead"         , po::value<double>()->default_value(25.0)                              , "Default camera look ahead.")
        (ALGO_NS "camera_trail_distance"     , po::value<double>()->default_value(25.0)                              , "Default camera trail distance.")
        ;

    Voyager::options(opt);
    FrameDebugger::options(opt);
    Statistics::options(opt);
}

void Algorithm::configure(const boost::program_options::variables_map& vm)
{
    street_width_               = vm[ALGO_NS "street_width"].as<double>();
    street_color_               = vm[ALGO_NS "street_color"].as<string>();
    background_color_           = vm[ALGO_NS "background_color"].as<string>();
    route_width_                = vm[ALGO_NS "route_width"].as<double>();
    route_color_                = vm[ALGO_NS "route_color"].as<string>();

    pointer_size_               = vm[ALGO_NS "pointer_size"].as<double>();
    pointer_color_              = vm[ALGO_NS "pointer_color"].as<string>();
    pointer_border_color_       = vm[ALGO_NS "pointer_border_color"].as<string>();
    pointer_elevation_          = vm[ALGO_NS "pointer_elevation"].as<double>();

    label_font_path_            = vm[ALGO_NS "label_font_path"].as<string>();
    label_font_size_            = vm[ALGO_NS "label_font_size"].as<double>();
    label_font_color_           = vm[ALGO_NS "label_font_color"].as<string>();
    label_draw_box_             = vm[ALGO_NS "label_draw_box"].as<bool>();
    label_fill_box_             = vm[ALGO_NS "label_fill_box"].as<bool>();
    label_box_background_color_ = vm[ALGO_NS "label_box_background_color"].as<string>();
    label_box_border_color_     = vm[ALGO_NS "label_box_border_color"].as<string>();
    label_box_margin_           = vm[ALGO_NS "label_box_margin"].as<double>();

    camera_height_              = vm[ALGO_NS "camera_height"].as<double>();
    camera_look_ahead_          = vm[ALGO_NS "camera_look_ahead"].as<double>();
    camera_trail_distance_      = vm[ALGO_NS "camera_trail_distance"].as<double>();

    voyager_.configure(vm);
    debugger_.configure(vm);
    stat.configure(vm);
}

int Algorithm::run(const straph::Straph* graph, const straph::Route* route)
{
    if (route == nullptr && graph == nullptr) {
        LOG4CXX_ERROR(logger, "Street graph and route are both empty! Exiting...");
        return EXIT_FAILURE;
    } else if (route == nullptr) {
        return view_network(graph);
    } else {
        return route_network(graph, route);
    }
}

/*
 * Just view the street network, without a route, pointer, or anything.
 * Any inheriting sub-algorithms won't be doing anything here.
 *
 * Note: this assumes that graph is non-null.
 */
int Algorithm::view_network(const straph::Straph* graph)
{
    assert(graph != nullptr);

    LOG4CXX_INFO(logger, "Just viewing the street network this time...");

    // 1. Creating scene graph.
    ref_ptr<Group> root = new Group();
    StreetFactory sf(street_width_, street_color_, route_width_, route_color_);
    root->addChild(sf.createStreets(*graph));
    optimize_scene(root.get());

    // 2. Setting up viewer.
    osgViewer::Viewer viewer;
    viewer.setSceneData(root.get());
    viewer.getCamera()->setClearColor(background_color_.vec4());
    viewer.setThreadingModel(osgViewer::ViewerBase::ThreadPerCamera);
    viewer.addEventHandler(new osgViewer::WindowSizeHandler);
    viewer.addEventHandler(new osgViewer::StatsHandler);
    viewer.addEventHandler(new osgViewer::HelpHandler);
#ifdef ENABLE_ANTIALIASING
    DisplaySettings::instance()->setNumMultiSamples(4);
#endif

    // 3. Running viewer.
    viewer.run();
    return EXIT_SUCCESS;
}

/*
 * Take the voyager through the route, with or without the rest of the
 * street network. Pointer and labels are present depending on the
 * algorithm.
 *
 * Note: this assumes that graph may be null, but route is non-null.
 */
int Algorithm::route_network(const straph::Straph* graph, const straph::Route* route)
{
    assert(route != nullptr);
    if (!straph::route_valid(*route) || route->empty()) {
        LOG4CXX_ERROR(logger, "Route is invalid or empty! Exiting...");
        return EXIT_FAILURE;
    }

    LOG4CXX_INFO(logger, "Routing through the street network...");

    // 1. Creating scene graph.
    // We optimize the scene right after setting the street network because
    // the other elements are anyway dynamic.
    ref_ptr<Group> root;
    StreetFactory sf(street_width_, street_color_, route_width_, route_color_);
    if (graph == nullptr) {
        LOG4CXX_DEBUG(logger, "No street network present, showing only route.");
        root = new Group();
        root->addChild(sf.createRoute(*route));
    } else {
        root = sf.createNetwork(*graph, *route);
    }
    optimize_scene(root.get());

    osgViewer::Viewer viewer;
    auto tracker = router_initialize(viewer, root.get(), route);
    if (tracker == nullptr) {
        return EXIT_FAILURE;
    }
    viewer.setSceneData(root.get());

    viewer.realize();
    viewer.frame();
    router_startup();
    while (!viewer.done()) {
        viewer.frame();
        bool ok = router_next(viewer, root.get());
        if (!ok) {
            break;
        }
    }
    return router_shutdown();
}

osgGA::NodeTrackerManipulator* Algorithm::router_initialize(osgViewer::Viewer& viewer, osg::Group* root, const straph::Route* route)
{
    assert(route != nullptr);
    assert(straph::route_valid(*route));
    assert(!route->empty());

    // Creating pointer and voyager.
    // Initializing the voyager will also set the starting position of the pointer.
    ref_ptr<Pointer> pointer{new Pointer(pointer_size_, pointer_color_, pointer_border_color_)};
    pointer->setElevation(pointer_elevation_);
    voyager_.initialize(*route, pointer.get());
    root->addChild(voyager_.getFollower());
    root->addChild(pointer.get());

    // Call algorithm hook.
    debugger_.initialize();
    bool ok = setup_hook(root, *route, voyager_);
    if (!ok) {
        LOG4CXX_INFO(logger, "Setup failed. Exiting.");
        return nullptr;
    }

    // Set up the scene viewer.
    // The node tracker will follow the pointer as it moves along the track.
    viewer.getCamera()->setClearColor(background_color_.vec4());
    viewer.setThreadingModel(osgViewer::ViewerBase::ThreadPerCamera);
    viewer.addEventHandler(new osgViewer::WindowSizeHandler);
    viewer.addEventHandler(new osgViewer::StatsHandler);
    viewer.addEventHandler(new osgViewer::HelpHandler);
#ifdef ENABLE_ANTIALIASING
    DisplaySettings::instance()->setNumMultiSamples(4);
#endif

    ref_ptr<osgGA::NodeTrackerManipulator> tracker{new osgGA::NodeTrackerManipulator()};
    tracker->setTrackNode(voyager_.getFollower());
    Vec3d cam_eye(0.0, -camera_trail_distance_, camera_height_);
    Vec3d cam_look(0.0, camera_look_ahead_, 0.0);
    Vec3d cam_up(0.0, 0.0, 1.0);
    tracker->setHomePosition(cam_eye, cam_look, cam_up);

    viewer.setCameraManipulator(tracker.get());
    viewer.addEventHandler(createVoyagerController(&voyager_));

    stat.route_length = route->size();
    return tracker.get();
}

void Algorithm::router_startup()
{
    total_timer_.reset();
}

bool Algorithm::router_next(osgViewer::Viewer& viewer, osg::Group* root)
{
    bool ok;
    Timer frame_timer;
    stat.total_frames++;

    // Find out if the debugger wants to write a frame.
    // If debug_ is true, then the algorithm might write values to debug_io_,
    // so we have to open it now before the
    debugger_.next();
    debug_ = debugger_.ready();
    if (debug_) {
        assert(!debug_io_);
        debug_io_.open(debugger_.filename(".txt"));
    }

    // Run the algorithm for the frame.
    {
        Timer algorithm_timer(&stat.total_time_algorithm, true);
        ok = voyager_.update();
        ok &= update_hook(root, viewer, voyager_);
    }

    if (debug_) {
        // debugging_timer stops on destruction at end of scope.
        Timer debugging_timer(&stat.total_time_debugging, true);
        svg_hook(root, viewer, debugger_.filename());
        debug_io_.close();
    }

    // Update time spent rotating, if we in fact did rotate.
    // Time spent rotating includes time for debugging and algorithm.
    if (voyager_.didRotate()) {
        stat.total_time_rotating += frame_timer.stop();
    }

    return ok;
}

int Algorithm::router_shutdown()
{
    stat.total_time = total_timer_.stop();
    return shutdown_hook();
}

int Algorithm::shutdown_hook()
{
    stat.report();
    return EXIT_SUCCESS;
}

bool Algorithm::setup_hook(osg::Group*, const straph::Route&, const Voyager&) { return true; }
bool Algorithm::update_hook(osg::Group*, const osgViewer::Viewer&, const Voyager&) { return true; }
void Algorithm::svg_hook(osg::Group*, osgViewer::Viewer&, const std::string) {}

} // namespace asl
