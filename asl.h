/**
 * \file asl.h
 *
 * The ASL library is all about active street labeling. To this end,
 * there are multiple algorithms. In order to simplify things, we have
 * one base class Algorithm which all the other algorithms need to
 * inherit from.
 *
 * \author Ben Morgan <benjamin.morgan@stud-mail.uni-wuerzburg.de>
 * \date   6. February 2015
 */

#ifndef INCLUDE_GUARD_DA170ACE8B054ECCB608285C7B1A6015
#define INCLUDE_GUARD_DA170ACE8B054ECCB608285C7B1A6015

#include "voyager.h"
#include "debugger.h"
#include "util/color.h"
#include "util/filewriter.h"
#include "util/timer.h"

#include <string>

#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>

#include <straph/straph.h>

#include <osg/Group>
#include <osgViewer/Viewer>
#include <osgGA/NodeTrackerManipulator>

namespace asl {

/**
 * The Algorithm class mostly implements the non-virtual interface idiom.
 *
 * A user of any derived algorithm should do the following:
 *
 *  1. All algorithms in the chain from the used implementation down to
 *     this interface should have their options() method called. This
 *     is a static method, so all could be done in one place.
 *  2. Before using the algorithm in question. the configure() method
 *     called.
 *  3. The method run() can be called with any combination of valid
 *     or null pointers. All cases are handled. If only the route
 *     pointer is non-null, then only the route is displayed. If the
 *     route is null, then routing is not performed.
 *
 * An implementer of the Algorithm interface, on the other hand, need only
 * implement the few virtual methods that there are in this class, if at all.
 * (That is, not all virtual methods need to be redefined.)
 */
class Algorithm {
public:
    Algorithm() {}
    virtual ~Algorithm() {}

    /**
     * When configuring the program, options lets the Algorithm state which
     * configuration options it accepts. This must be called.
     */
    static void options(boost::program_options::options_description& opt);

    /**
     * Each algorithm needs to be configured. This method must be called prior
     * to run(). If it is not, the behavior is undefined.
     */
    virtual void configure(const boost::program_options::variables_map& config);

    /**
     * Run the algorithm, given that options() and configure() have been called.
     * It returns the result from shutdown_hook, which is the exit code.
     */
    int run(const straph::Straph* graph, const straph::Route* route);

    // Alternative hooks if you want to maintain control yourself.
    // See route_network(*) function for how it is done.
    osgGA::NodeTrackerManipulator* router_initialize(osgViewer::Viewer& viewer, osg::Group* root, const straph::Route* route);
    void router_startup();
    bool router_next(osgViewer::Viewer& viewer, osg::Group* root);
    int router_shutdown();

protected:

    /**
     * After creating the scene graph and initialising voyager, this hook is called.
     */
    virtual bool setup_hook(osg::Group* scene, const straph::Route& route, const Voyager& voyager);

    /**
     * Once per frame, after the voyager has updated itself, this hook is called.
     */
    virtual bool update_hook(osg::Group* scene, const osgViewer::Viewer& viewer, const Voyager& voyager);

    /**
     * Depending on the configuration, this hook may be called to output an SVG debugging output.
     * The input is the path to the file. It is called after the update hook.
     */
    virtual void svg_hook(osg::Group* scene, osgViewer::Viewer& viewer, const std::string path);

    /**
     * At the end of the algorithm, right before shutdown, this hook is called.
     */
    virtual int shutdown_hook();

protected:
// data members:
    double street_width_;
    Color street_color_;
    Color background_color_;

// routing:
    double route_width_;
    Color route_color_;

// pointer:
    double pointer_size_;
    Color pointer_color_;
    Color pointer_border_color_;
    double pointer_elevation_;

// labeling:
    std::string label_font_path_;
    double label_font_size_;
    Color label_font_color_;
    bool label_draw_box_;
    bool label_fill_box_;
    Color label_box_background_color_;
    Color label_box_border_color_;
    double label_box_margin_;
    bool label_draw_leader_;

// camera:
    double camera_height_;
    double camera_look_ahead_;
    double camera_trail_distance_;

// extra:
    // You may be tempted to remove this, as it _seems_ that it's only
    // in one function, but it's needed here for configuration. Sorry.
    Voyager voyager_;
    FrameDebugger debugger_;
    FileWriter debug_io_;
    Timer total_timer_;
    bool debug_;

private:
    int view_network(const straph::Straph* graph);
    int route_network(const straph::Straph* graph, const straph::Route* route);
};

} // namespace asl
#endif // INCLUDE_GUARD
