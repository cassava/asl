#!/usr/bin/perl

use 5.014;
use warnings;
use Switch;

sub color {
    switch ($1) {
        case 'Black' { return "000000"; }
        case 'White' { return "ffffff"; }
        case 'Butter1' { return "fce94f"; }
        case 'Butter2' { return "edd400"; }
        case 'Butter3' { return "c4a000"; }
        case 'Orange1' { return "fcaf3e"; }
        case 'Orange2' { return "f57900"; }
        case 'Orange3' { return "ce5c00"; }
        case 'Chocolate1' { return "e9b96e"; }
        case 'Chocolate2' { return "c17d11"; }
        case 'Chocolate3' { return "8f5902"; }
        case 'Chameleon1' { return "73d216"; }
        case 'Chameleon2' { return "4e9a06"; }
        case 'Chameleon3' { return "8ae234"; }
        case 'SkyBlue1' { return "3465a4"; }
        case 'SkyBlue2' { return "204a87"; }
        case 'SkyBlue3' { return "729fcf"; }
        case 'Plum1' { return "ad7fa8"; }
        case 'Plum2' { return "75507b"; }
        case 'Plum3' { return "5c3566"; }
        case 'Scarlet1' { return "ef2929"; }
        case 'Scarlet2' { return "cc0000"; }
        case 'Scarlet3' { return "a40000"; }
        case 'Aluminium1' { return "eeeeec"; }
        case 'Aluminium2' { return "d3d7ef"; }
        case 'Aluminium3' { return "babdb6"; }
        case 'Aluminium4' { return "888a85"; }
        case 'Aluminium5' { return "555753"; }
        case 'Aluminium6' { return "2e3436"; }
    }
}

my $configs = `grep -hE ALGO_NS @ARGV 2>/dev/null`;

for (split /^/, $configs) {
    if (/\s*#define ALGO_NS "(.*)\."/) {
        print "\n";
        print "[$1]\n";
    } elsif (/\s*\(ALGO_NS "([^,"]+)[^"]*".*,\s*po::value<(.+)>\(\).*,\s*"(.+)"\)/) {
        my $name = $1;
        my $type = $2;
        my $help = $3;

        if (/po::value<.*>\(\)->default_value\(['"]?([^)'"]*)['"]?\)/) {
            my $default = $1;

            if ($default =~ /TangoColor::(.+)/) {
                $default = &color($1);
            }

            print "## $name: $type (default=$default)\n";
            print "## $help\n";
            print "$name = $default\n\n";
        } else {
            print "## $name: $type\n";
            print "## $help\n";
            print "$name =\n\n";
        }
    }
}
