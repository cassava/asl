#!/bin/sh

grep -R '#define INCLUDE_GUARD_' $@ | sed -r 's/.*(INCLUDE_GUARD_.+)/\1/' | sort | uniq -d
