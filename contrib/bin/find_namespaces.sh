#!/bin/sh

# find_namespaces.sh
#
# This little script finds all the log4cxx namespaces that are being used in
# the project, apart from the ones used in the straph library.
#
# Run it in the main directory.

grep -R 'getL[o]gger(' $@ | sed -r 's/.*getLogger\("([^"]+)".*/\1/'
