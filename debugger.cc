/*
 * debugger.cc
 *
 * Ben Morgan <benjamin.morgan@stud-mail.uni-wuerzburg.de>
 * 04. December 2014
 */

#include "debugger.h"

#include <fstream>
#include <iomanip>
#include <ostream>
#include <sstream>
#include <string>
using namespace std;

#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/filesystem.hpp>
using namespace boost::filesystem;

#include <log4cxx/logger.h>

// ALGO_NS is the configuration namespace for this class.
#define ALGO_NS "debugger."

namespace asl {

namespace {

log4cxx::LoggerPtr logger = log4cxx::Logger::getLogger("asl.debugger");

string next_nonexisting_dir(const string& dir_prefix, unsigned dir_padding)
{
    string dirname;
    for (unsigned dir_count = 1; true; dir_count++) {
        ostringstream stream;
        stream << dir_prefix << setw(dir_padding) << setfill('0') << dir_count;
        if (!exists(stream.str())) {
            dirname = stream.str();
            break;
        }
    }
    return path(dirname).string();
}

} // anonymous namespace

void FrameDebugger::options(boost::program_options::options_description& opt)
{
    namespace po = boost::program_options;

    opt.add_options()
        (ALGO_NS "enabled"           , po::value<bool>()->default_value(false)     , "If enabled, write SVG files every few frames. This shows how the algorithm sees the world.")
        (ALGO_NS "frequency"         , po::value<unsigned>()->default_value(1)     , "Write SVG files every n frames. Setting to 1 means every frame, 2 means every second frame, 10 means every tenth frame, etc.")
        (ALGO_NS "directory_prefix"  , po::value<string>()->default_value("")      , "Directory prefix where SVG files are written. If a directory already exists, then a number is appended until no directory has that name.")
        (ALGO_NS "directory_padding" , po::value<unsigned>()->default_value(2)     , "Default amount to pad directory count; that is, if it is 2, then numbers are written like 01, 02, and so on. If it is 4, then 0001, 0002, etc.")
        (ALGO_NS "file_prefix"       , po::value<string>()->default_value("frame-"), "Default file prefix for SVG files.")
        (ALGO_NS "file_suffix"       , po::value<string>()->default_value(".svg")  , "Default file suffix for SVG files. Should be '.svg'.")
        (ALGO_NS "file_padding"      , po::value<unsigned>()->default_value(6)     , "Default amount to pad file count. Same idea as with directory_padding. A good value here is 6.")
        (ALGO_NS "file_limit"        , po::value<size_t>()->default_value(1000)    , "Write at the most this many SVG files. Set to 0 for infinite.")
        ;
}

void FrameDebugger::configure(const boost::program_options::variables_map& vm)
{
    enabled_      = vm[ALGO_NS "enabled"].as<bool>();
    frequency_    = vm[ALGO_NS "frequency"].as<unsigned>();
    dir_prefix_   = vm[ALGO_NS "directory_prefix"].as<string>();
    dir_padding_  = vm[ALGO_NS "directory_padding"].as<unsigned>();
    file_prefix_  = vm[ALGO_NS "file_prefix"].as<string>();
    file_suffix_  = vm[ALGO_NS "file_suffix"].as<string>();
    file_padding_ = vm[ALGO_NS "file_padding"].as<unsigned>();
    file_limit_   = vm[ALGO_NS "file_limit"].as<size_t>();
    frame_        = 0;

    if (frequency_ == 0) {
        enabled_ = false;
    }
}

void FrameDebugger::initialize()
{
    if (!enabled_) {
        return;
    }

    dir_ = next_nonexisting_dir(dir_prefix_, dir_padding_);
    LOG4CXX_INFO(logger, "Creating directory " << dir_ << " for writing SVG files.");
    try {
        create_directory(dir_);
    } catch (exception e) {
        LOG4CXX_ERROR(logger, "Error creating directory " << dir_ << ": " << e.what() << ".");
        enabled_ = false;
        return;
    }
}

bool FrameDebugger::ready()
{
    // There are a lot of reasons to skip this, here we go:
    if (!enabled_ || frame_%frequency_ != 0) {
        return false;
    } else if (file_limit_ != 0 && frame_/frequency_ >= file_limit_) {
        LOG4CXX_WARN(logger, "Frame debugger file limit reached!");
        enabled_ = false;
        return false;
    }
    return true;
}

std::string FrameDebugger::filename(const std::string& suffix)
{
    path p = dir_;
    std::ostringstream stream;
    stream << file_prefix_ << setw(file_padding_) << setfill('0') << frame_ << suffix;
    p /= stream.str();
    return p.string();
}

} // namespace asl
