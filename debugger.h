/**
 * \file debugger.h
 *
 * \author Ben Morgan <benjamin.morgan@stud-mail.uni-wuerzburg.de>
 * \date   04. November 2014
 */
#ifndef INCLUDE_GUARD_A7592FA3B745474AAA0B1533E0F2229C
#define INCLUDE_GUARD_A7592FA3B745474AAA0B1533E0F2229C

#include <string>

// Forward declaration optimization
namespace boost {
    namespace program_options {
        // #include <boost/program_options/options_description.hpp>
        class options_description;
        // #include <boost/program_options/variables_map.hpp>
        class variables_map;
    }
}

namespace asl {

class FrameDebugger {
public:
    FrameDebugger() {}
    ~FrameDebugger() {}

    static void options(boost::program_options::options_description& opt);
    void configure(const boost::program_options::variables_map& config);
    void initialize();

    /// next advances the frame count, which is used by the ready function to
    /// determine if the time is right for another debugging frame, and the
    /// filename function to construct the name of the file to be written.
    void next() {
        frame_++;
    }

    /// ready returns true when a new frame should be written.
    bool ready();

    std::string filename() {
        return filename(file_suffix_);
    }

    std::string filename(const std::string& suffix);

private:
    bool        enabled_;
    unsigned    frequency_;

    std::string dir_prefix_;
    unsigned    dir_padding_;
    std::string file_prefix_;
    std::string file_suffix_;
    unsigned    file_padding_;

    std::size_t file_limit_;
    std::size_t frame_;

    std::string dir_;
};

} // namespace asl
#endif // INCLUDE_GUARD
