/**
 * exponential_fd.cc
 *
 * Ben Morgan <benjamin.morgan@stud-mail.uni-wuerzburg.de>
 * 04. November 2014
 */

#include "exponential_fd.h"

namespace asl {

ExponentialLF::ExponentialLF(ExponentialFDA* fda, Label* l) : LinearLF(fda, l) {}

double ExponentialLF::repulsion_force(const ScreenSpace& s) const
{
    if (space.xintersects(s)) {
        if (space.yintersects(s)) {
            double o = 1.0 - space.overlap(s)/static_cast<double>(space.area());
            double f = 1 / (o * o);
            return space.ysign(s) * (f > conf->force_limit_ ? conf->force_limit_ : f);
        }
        double d = 1 + space.yoffset(s)/space.height();
        return space.ysign(s) / (d*d);
    }
    return 0.0;
}

} // namespace asl
