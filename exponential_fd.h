/**
 * \file exponential_fd.h
 *
 * \author Ben Morgan <benjamin.morgan@stud-mail.uni-wuerzburg.de>
 * \date   25. October 2014
 */

#ifndef INCLUDE_GUARD_A69010E7CC0D48E2B284E7A14262D424
#define INCLUDE_GUARD_A69010E7CC0D48E2B284E7A14262D424

#include "linear_fd.h"

namespace asl {

class ExponentialFDA;

class ExponentialLF : public LinearLF {
public:
    ExponentialLF(ExponentialFDA* fda, Label* l);
    ~ExponentialLF() {}

    double repulsion_force(const ScreenSpace& s) const;
};

class ExponentialFDA : public LinearFDA {
public:
    friend ExponentialLF;

    ExponentialFDA() {}
    ~ExponentialFDA() {}

    static void options(boost::program_options::options_description&) {}

protected:
    LabelForce* new_labelforce(Label* lab) {
        return new ExponentialLF(this, lab);
    }
};

} // namespace asl
#endif // INCLUDE_GUARD
