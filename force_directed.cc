/**
 * force_directed.cc
 *
 * Ben Morgan <benjamin.morgan@stud-mail.uni-wuerzburg.de>
 * 19. December 2014
 */

#include "force_directed.h"
#include "statistics.h"
#include "util/draw.h"
#include "util/screenspace.h"
#include "util/svg.h"

#include <cassert>
#include <list>
using namespace std;

#include <osg/Camera>
#include <osg/Group>
using namespace osg;

#include <log4cxx/logger.h>

// ALGO_NS is the configuration namespace for this class.
#define ALGO_NS "algorithm.force_directed."

namespace asl {

namespace {

log4cxx::LoggerPtr logger = log4cxx::Logger::getLogger("asl.algorithm.force-directed");

} // anonymous namespace

bool ForceDirectedAlgorithm::setup_hook(osg::Group* scene, const straph::Route& route, const Voyager& voyager)
{
    bool ok = StaticAlgorithm::setup_hook(scene, route, voyager);
    if (!ok) {
        return false;
    }

    for (Label* lab : active_labels_) {
        label_forces_.push_back(shared_ptr<LabelForce>(new_labelforce(lab)));
    }
    return true;
}

bool ForceDirectedAlgorithm::update_hook(osg::Group* scene, const osgViewer::Viewer& viewer, const Voyager& voyager)
{
    bool ok = StaticAlgorithm::update_hook(scene, viewer, voyager);

    Translator t(viewer.getCamera());
    ScreenSpace screen = t.screen();

    list<LabelForce*> relevant_forces;
    for (shared_ptr<LabelForce>& lf : label_forces_) {
        lf->update(t);
        const ScreenSpace& s = lf->getScreenSpace();
        // Note: in the following, I don't check if stat.enabled,
        // because this would be more work than incrementing the variable.
        if (s.behind()) {
            lf->reset();
            stat.total_ignored++;
            continue;
        } else {
            unsigned a = s.area();
            if (a < insig_threshold_) {
                stat.total_ignored++;
                continue;
            } else if (a > screen.area() / 4) {
                stat.total_ignored++;
                continue;
            }
        }

        relevant_forces.push_back(lf.get());
    }

    for (unsigned i = relevant_forces.size(); i > 0; --i) {
        LabelForce* lf = relevant_forces.front();
        relevant_forces.pop_front();

        lf->animate(relevant_forces);
        assert(lf->isValid());

        relevant_forces.push_back(lf);
    }

    return ok;
}

void ForceDirectedAlgorithm::show_label_hook(Label* label)
{
#ifdef ENABLE_EXPERIMENTAL_LABEL_ENTRY
    // We set the new label's height to the maximum of all the active labels,
    // so that it doesn't appear beneath existing active labels.
    // Because the label is farther away, with the same height it would be below
    // the previous label, which would not achieve what we are trying to achieve.
    // This is why we add 1.0 to the maximum height.
    double maxh = 0.0;
    for (Label* l : active_labels_) {
        if (l->getHeight() > maxh) {
            maxh = l->getHeight();
        }
    }
    label->setHeight(maxh + 1.0);
#endif

    StaticAlgorithm::show_label_hook(label);
    label_forces_.push_back(shared_ptr<LabelForce>(new_labelforce(label)));
}

void ForceDirectedAlgorithm::hide_label_hook(Label* label)
{
    StaticAlgorithm::hide_label_hook(label);
    label_forces_.pop_front();
}

void ForceDirectedAlgorithm::svg_hook(osg::Group*, osgViewer::Viewer& viewer, const std::string path)
{
    Translator t(viewer.getCamera());
    ScreenSpace screen = t.screen();
    SVGWriter w(path, screen);
#ifdef ENABLE_IMAGE_DEBUGGING
    w << svg::background(viewer);
#endif

    const int x = 5;
    const int text_height = 25;
    unsigned row = 0;
    for (auto rit = label_forces_.rbegin(); rit != label_forces_.rend(); ++rit) {
        const shared_ptr<LabelForce>& lf = *rit;
        if (lf->getScreenSpace().intersects(screen)) {
            int y = ++row * text_height;
            w << *lf << svg::text(x, y, lf->str());
        }
    }
}

} // namespace asl
