/**
 * \file force_directed.h
 *
 * \author Ben Morgan <benjamin.morgan@stud-mail.uni-wuerzburg.de>
 * \date   19. December 2014
 */

#ifndef INCLUDE_GUARD_EF5E95532C454FB3B61EAAE6B768E745
#define INCLUDE_GUARD_EF5E95532C454FB3B61EAAE6B768E745

#include "asl.h"
#include "static.h"
#include "util/draw.h"
#include "util/screenspace.h"
#include "util/svg.h"

#include <cassert>
#include <functional>
#include <list>

#include <osg/Camera>
#include <osg/Group>

namespace asl {

class LabelForce : public SVG {
public:
    LabelForce(Label* l) : label(l) {}
    virtual ~LabelForce() {}

    static void options(boost::program_options::options_description& opt);
    static void configure(const boost::program_options::variables_map& config);

    const Label* getLabel() const { return label; }
    const ScreenSpace& getScreenSpace() const { return space; }

    virtual bool isValid() const = 0;
    virtual std::string str() const = 0;

    virtual void reset() = 0;
    virtual void update(const Translator& t) = 0;

    // The passed labels are guaranteed to be at least of the same super-class
    // of the implementing method, so it is safe for an implementation to
    // dynamically cast the LabelForce to its own class type.
    virtual void animate(const std::list<LabelForce*>& labels) = 0;

    // Implementation for SVG interface
    virtual void svg(std::ostream& os, const ScreenSpace& screen, const std::string& path) const = 0;

protected:
    Label*      label;
    ScreenSpace space;
};

class ForceDirectedAlgorithm : public StaticAlgorithm {
public:
    ForceDirectedAlgorithm() {}
    virtual ~ForceDirectedAlgorithm() {}

    static void options(boost::program_options::options_description&) {}

protected:
    virtual bool setup_hook(osg::Group* scene, const straph::Route& route, const Voyager& voyager);
    virtual bool update_hook(osg::Group* scene, const osgViewer::Viewer& viewer, const Voyager& voyager);
    virtual void show_label_hook(Label* label);
    virtual void hide_label_hook(Label* label);
    virtual void svg_hook(osg::Group* scene, osgViewer::Viewer& viewer, const std::string path);

    // All subclasses of this class need to implement this function to return
    // a new subclass of LabelForce.
    virtual LabelForce* new_labelforce(Label* lab) = 0;

protected:
// data members:
    std::list<std::shared_ptr<LabelForce>> label_forces_;
};

} // namespace asl
#endif // INCLUDE_GUARD
