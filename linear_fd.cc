/*
 * linear_fd.cc
 *
 * Ben Morgan <benjamin.morgan@stud-mail.uni-wuerzburg.de>
 * 19. December 2014
 */

#include "linear_fd.h"
#include "statistics.h"

#include <cassert>
#include <limits>
#include <string>
#include <sstream>
using namespace std;

#include <log4cxx/logger.h>

// ALGO_NS is the configuration namespace for this class.
#define ALGO_NS "algorithm.force_directed."

namespace asl {

namespace {

log4cxx::LoggerPtr logger = log4cxx::Logger::getLogger("asl.algorithm.force-directed");

} // anonymous namespace

bool LinearLF::isValid() const
{
    return conf != nullptr
        && label != nullptr
        && height >= 0.0
        && force == force                   // force is a number
        && uforce == uforce // is false if NaN
        && temp >= conf->temperature_base_
        ;
}

std::string LinearLF::str() const
{
    double h = height - change;
    double Fs = spring_force(h);
    double Fr = uforce - Fs;

    stringstream s;
    s << label->getName() << ": h=" << h << " ~> Fs=" << Fs << ", Fr=" << Fr << ", T=" << temp << ", C=" << change;
    return s.str();
}

void LinearLF::reset()
{
    force  = 0.0;
    temp   = 1.0;
    change = 0.0;
}

void LinearLF::update(const Translator& t)
{
    space = t.screenspace(label);
    space.margin_bottom(conf->label_margin_bottom_);
}

void LinearLF::animate(const list<LabelForce*>& labels)
{
    LOG4CXX_TRACE(logger, "Updating " << label->getName() << ":");

    double Fs = spring_force(height);
    double Fr = repulsion_forces(labels);
    double F = Fs + Fr;
    LOG4CXX_TRACE(logger, " -> force is " << F << " = " << Fs << " + " << Fr);
    if (F != F) {
        LOG4CXX_TRACE(logger, " -> force is not a number!");
        reset();
        return;
    } else if (abs(F) < conf->sensitivity_threshold_) {
        LOG4CXX_TRACE(logger, " -> force below sensitivity threshold " << conf->sensitivity_threshold_ << "!");
        reset();
        return;
    } else if (abs(F) > conf->force_limit_) {
        LOG4CXX_TRACE(logger, " -> force limit " << conf->force_limit_ << " exceeded!");
        if (conf->enforce_limits_) {
            uforce = F;
            F = conf->force_limit_;
        }
    }

    double T = temperature(F);
    LOG4CXX_TRACE(logger, " -> temperature is " << T);

    adjust(F, T);
    LOG4CXX_TRACE(logger, " => adjusting leader height by " << change);

    if (stat.enabled) {
        stat.total_spring_force += abs(Fs);
        stat.total_repulsive_force += abs(Fr);
        stat.total_force += abs(force);
        stat.total_temperature += temp;
        stat.total_change += abs(change);
    }
}

double LinearLF::spring_force(double h) const
{
    return -conf->spring_constant_ * (h - conf->leader_height_);
}

double LinearLF::repulsion_force(const ScreenSpace& s) const
{
    if (space.xintersects(s)) {
        if (space.yintersects(s)) {
            double o = space.overlap(s) / static_cast<double>(space.area());
            assert(o > 0.0);
            return space.ysign(s) * (conf->force_base_ + o*(conf->force_limit_ - conf->force_base_));
        }
        double d = space.yoffset(s);
        return space.ysign(s) * (conf->force_base_ - d*(conf->force_base_ / numeric_limits<double>::max()));
    }
    assert(!space.intersects(s));
    return 0.0;
}

double LinearLF::repulsion_forces(const list<LabelForce*>& labels) const
{
    double F = 0.0;
    for (const LabelForce* alt : labels) {
        F += repulsion_force(alt->getScreenSpace());
    }
    return conf->repulsion_constant_ * F;
}

double LinearLF::temperature(double F) const
{
    double T = temp;
#ifdef ENABLE_EXPERIMENTAL_LABEL_ENTRY
    if ((F < 0.0) == (force < 0.0) || force == 0.0) {
#else
    if ((F < 0.0) == (force < 0.0)) {
#endif
        T *= conf->temperature_step_;
        if (T > conf->temperature_limit_) {
            if (conf->enforce_limits_) {
                T = conf->temperature_limit_;
            }
        }
    } else {
        T = conf->temperature_base_;
    }
    return T;
}

double LinearLF::adjust(double F, double T)
{
    force = F, temp = T;
    change = force * temp * conf->beta_constant_;

    if (height + change < 0.0) {
        label->setHeight(0.0);
        change = height;
        height = 0.0;
    } else {
        label->adjustHeight(change);
        height += change;
    }
    return change;
}

namespace {

string svg_fill(double temp, double temp_limit)
{
    string color = "#babdb6"; // Aluminium3
    if (temp < 1.0) {
        if (temp < 0.25) {
            color = "#204a87"; // SkyBlue3
        } else if (temp < 0.5) {
            color = "#3465a4"; // SkyBlue2
        } else if (temp < 0.75) {
            color = "#729fcf"; // SkyBlue1
        } else {
            color = "#ad7fa8"; // Plum1
        }
    } else if (temp > 1.0) {
        temp_limit -= 1.0;
        temp -= 1.0;
        if (temp > temp_limit) {
            color = "#a40000"; // Scarlet3
        } else if (temp > temp_limit/2) {
            color = "#cc0000"; // Scarlet2
        } else {
            color = "#ef2929"; // Scarlet1
        }
    }
    return color;
}

// A rotation in (0,90) is upwards, and between (90,180) is downwards.
double svg_rotation(double force, double force_limit)
{
    if (force >= force_limit) {
        return 0.0;
    } else if (-1.0*force >= force_limit) {
        return 180.0;
    }
    return 90.0 - 90.0*(force/force_limit);
}

} // anonymous namespace

void LinearLF::svg(std::ostream& os, const ScreenSpace& screen, const std::string& path) const
{
    os << endl;

    const ScreenSpace& s = space;
    unique_ptr<svg::Rect> r(new svg::Rect(s));

    if (abs(force) < conf->sensitivity_threshold_) {
        r->svg(os, screen, path);
        return;
    }

    // Set the background color based on the temperature
    r->setFillColor( svg_fill(temp, conf->temperature_limit_) );
    r->svg(os, screen, path);

    // Set the fill angle based on the force
    double rotation = svg_rotation(force, conf->force_limit_);
    if (rotation == 0.0 || rotation == 180.0) {
        svg::FlyingRect fr(s);
        fr.setFlyerBelow(rotation == 180.0);
        fr.svg(os, screen, path);
    } else {
        svg::HatchedRect hr(s);
        hr.setHatchAngle(rotation);
        hr.svg(os, screen, path);
    }
}

void LinearFDA::options(boost::program_options::options_description& opt)
{
    namespace po = boost::program_options;

    opt.add_options()
        (ALGO_NS "label_margin_bottom"     , po::value<int>()->default_value(2)       , "Bottom label margin in pixels. This artificially extends the screen space of a label. When they are not overlapping in the algorithm, they have at least margin pixels between them.")
        (ALGO_NS "spring_constant"         , po::value<double>()->default_value(0.1)  , "Spring constant exerted on leader extension. According to Hooke's Law: Fs = -k * extension.")
        (ALGO_NS "repulsion_constant"      , po::value<double>()->default_value(1.0)  , "Force with which labels repulse each other. This is just scales the repulsive force.")
        (ALGO_NS "beta_constant"           , po::value<double>()->default_value(0.25) , "Blackbox constant to affect leader adjustment. This just scales the force. Increasing this speeds up label movement.")
        (ALGO_NS "force_base"              , po::value<double>()->default_value(0.1)  , "Force that labels have when they are touching but not overlapping. This value does not have an effect in spring_matching_fd algorithm.")
        (ALGO_NS "force_limit"             , po::value<double>()->default_value(5.0)  , "Maximum force an occlussion between two labels can affect.")
        (ALGO_NS "temperature_step"        , po::value<double>()->default_value(1.05) , "This factor is multiplied to make the temperature rise. Hence it must be > 1.")
        (ALGO_NS "temperature_base"        , po::value<double>()->default_value(0.10) , "Base temperature for a label after reset.")
        (ALGO_NS "temperature_limit"       , po::value<double>()->default_value(5.0)  , "Temperature cannot grow more than this, if enforce_limits is set to true.")
        (ALGO_NS "enforce_limits"          , po::value<bool>()->default_value(false)  , "Somewhat unclean at the moment. For some algorithms, this makes force_limit actually work. For other algorithms, it caps the force at the end again. It also makes temperature_limit work.")
        (ALGO_NS "sensitivity_threshold"   , po::value<double>()->default_value(0.01) , "The point at which the force of a label is ignored.")
        ;
}

void LinearFDA::configure(const boost::program_options::variables_map& vm)
{
    ForceDirectedAlgorithm::configure(vm);

    label_margin_bottom_   = vm[ALGO_NS "label_margin_bottom"].as<int>();
    spring_constant_       = vm[ALGO_NS "spring_constant"].as<double>();
    repulsion_constant_    = vm[ALGO_NS "repulsion_constant"].as<double>();
    beta_constant_         = vm[ALGO_NS "beta_constant"].as<double>();
    force_base_            = vm[ALGO_NS "force_base"].as<double>();
    force_limit_           = vm[ALGO_NS "force_limit"].as<double>();
    temperature_step_      = vm[ALGO_NS "temperature_step"].as<double>();
    temperature_base_      = vm[ALGO_NS "temperature_base"].as<double>();
    temperature_limit_     = vm[ALGO_NS "temperature_limit"].as<double>();
    enforce_limits_        = vm[ALGO_NS "enforce_limits"].as<bool>();
    sensitivity_threshold_ = vm[ALGO_NS "sensitivity_threshold"].as<double>();

    // A few assumptions that we are making:
    assert(label_margin_bottom_ >= 0);
    assert(force_limit_ > 1.0);
    assert(force_base_ > 0.0 && force_base_ < 1.0);
    assert(temperature_step_ > 1.0);
    assert(temperature_base_ < 1.0);
    assert(temperature_limit_ > 1.0);
    assert(sensitivity_threshold_ < 1.0);
}

} // namespace asl
