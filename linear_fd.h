/**
 * \file linear_fd.h
 *
 * \author Ben Morgan <benjamin.morgan@stud-mail.uni-wuerzburg.de>
 * \date   19. December 2014
 */

#ifndef INCLUDE_GUARD_B5107A4DDB9B425690F3DEC7117E5647
#define INCLUDE_GUARD_B5107A4DDB9B425690F3DEC7117E5647

#include "force_directed.h"

namespace asl {

class LinearFDA;
class QuadraticLF;
class ExponentialLF;
class SpringMatchingLF;

class LinearLF : public LabelForce {
public:
    LinearLF(LinearFDA* fda, Label* l)
        : LabelForce(l)
        , height(l->getHeight())
        , force(0.0)
        , uforce(0.0)
        , temp(1.0)
        , change(0.0)
        , conf(fda)
    {}

    virtual ~LinearLF() {}

    virtual bool isValid() const;
    virtual std::string str() const;

    virtual void reset();
    virtual void update(const Translator& t);

    virtual double spring_force(double h) const;
    virtual double repulsion_force(const ScreenSpace& s) const;
    virtual double repulsion_forces(const std::list<LabelForce*>& labels) const;
    virtual double temperature(double F) const;
    virtual double adjust(double F, double T);

    // The passed labels are guaranteed to be at least of the same super-class
    // of the implementing method, so it is safe for an implementation to
    // dynamically cast the LabelForce to its own class type.
    virtual void animate(const std::list<LabelForce*>& labels);

    // Implementation for SVG interface
    virtual void svg(std::ostream& os, const ScreenSpace& screen, const std::string& path) const;

protected:
    double     height;
    double     force;
    double     uforce;
    double     temp;
    double     change;
    LinearFDA* conf;
};

class LinearFDA : public ForceDirectedAlgorithm {
public:
    friend LinearLF;
    friend QuadraticLF;
    friend ExponentialLF;
    friend SpringMatchingLF;

    LinearFDA() {}
    virtual ~LinearFDA() {}

    static void options(boost::program_options::options_description& opt);
    virtual void configure(const boost::program_options::variables_map& vm);

protected:
    virtual LabelForce* new_labelforce(Label* lab) {
        return new LinearLF(this, lab);
    }

protected:
// data members:
    double label_margin_bottom_;
    double spring_constant_;
    double repulsion_constant_;
    double beta_constant_;
    double force_base_;
    double force_limit_;
    double temperature_step_;
    double temperature_base_;
    double temperature_limit_;
    bool   enforce_limits_;
    double sensitivity_threshold_;
};

} // namespace asl
#endif // INCLUDE_GUARD
