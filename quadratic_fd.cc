/**
 * quadratic_fd.cc
 *
 * Ben Morgan <benjamin.morgan@stud-mail.uni-wuerzburg.de>
 * 04. November 2014
 */

#include "quadratic_fd.h"

#include <limits>
using namespace std;

namespace asl {

QuadraticLF::QuadraticLF(QuadraticFDA* fda, Label* l) : LinearLF(fda, l) {}

double QuadraticLF::repulsion_force(const ScreenSpace& s) const
{
    if (space.xintersects(s)) {
        if (space.yintersects(s)) {
            double o = 1.0 - space.overlap(s)/static_cast<double>(space.area());
            double f = 1.0/(o * o) - (1.0 - conf->force_base_);
            return space.ysign(s) * (f > conf->force_limit_ ? conf->force_limit_ : f);
        }
        double d = space.yoffset(s);
        return space.ysign(s) * (conf->force_base_ - d*(conf->force_base_ / numeric_limits<double>::max()));
    }
    return 0.0;
}

} // namespace asl
