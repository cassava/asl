/**
 * \file quadratic_fd.h
 *
 * \author Ben Morgan <benjamin.morgan@stud-mail.uni-wuerzburg.de>
 * \date   24. October 2014
 */

#ifndef INCLUDE_GUARD_A8F1E3FF7CFD4A669516BB3E22AAA6DD
#define INCLUDE_GUARD_A8F1E3FF7CFD4A669516BB3E22AAA6DD

#include "linear_fd.h"

namespace asl {

class QuadraticFDA;

class QuadraticLF : public LinearLF {
public:
    QuadraticLF(QuadraticFDA* fda, Label* l);
    ~QuadraticLF() {}

    double repulsion_force(const ScreenSpace& s) const;
};

class QuadraticFDA : public LinearFDA {
public:
    friend QuadraticLF;

    QuadraticFDA() {}
    ~QuadraticFDA() {}

    static void options(boost::program_options::options_description&) {}

protected:
    LabelForce* new_labelforce(Label* lab) {
        return new QuadraticLF(this, lab);
    }
};

} // namespace asl
#endif // INCLUDE_GUARD
