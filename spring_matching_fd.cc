/**
 * spring_matching_fd.cc
 *
 * Ben Morgan <benjamin.morgan@stud-mail.uni-wuerzburg.de>
 * 05. November 2014
 */

#include "spring_matching_fd.h"
#include "statistics.h"

#include <cmath>
using namespace std;

#include <log4cxx/logger.h>

// A little debugging help
#define DEBUGIO if (conf->debug_) conf->debug_io_

namespace asl {

namespace {

log4cxx::LoggerPtr logger = log4cxx::Logger::getLogger("asl.algorithm.force-directed");

} // anonymous namespace

SpringMatchingLF::SpringMatchingLF(SpringMatchingFDA* fda, Label* l)
    : LinearLF(fda, l)
    , force_down(0.0)
    , force_up(0.0)
{}

std::string SpringMatchingLF::str() const
{
    double h = height - change;
    double Fs = spring_force(h);

    stringstream s;
    s << label->getName() << ": h=" << h << " ~> Fs=" << Fs << ", Fdown=" << force_down
      << ", Fup=" << force_up << ", T=" << temp << ", C=" << change;
    return s.str();
}

void SpringMatchingLF::animate(const std::list<LabelForce*>& labels)
{
    LOG4CXX_TRACE(logger, "Updating " << label->getName() << ":");
    DEBUGIO << "[" << label->getName() << "]\n"
            << "\tspace = " << space << "\n";

    // The main thing that we are doing differently from LinearLF is that we
    // are taking into account how much force comes from above and below
    // and deciding based on that whether we should include the leader force
    // or not.
    //
    // The difficulty is deciding when. Currently, when two labels are applying
    // forces on this label from different sides, we ignore the spring forces.
    double F = 0.0;
    double Fs = spring_force(height);
    DEBUGIO << "\th = " << height << "\n"
            << "\tFs = " << Fs << "\n";
    tie(force_down, force_up) = repulsion_forces2(labels);
    DEBUGIO << "\tFdown = " << force_down << "\n"
            << "\tFup = " << force_up << "\n";
    if (force_down == 0.0 || force_up == 0.0) {
        F = Fs + force_down + force_up;
        LOG4CXX_TRACE(logger, " -> force(3) is " << F << " = " << Fs << " + " << force_down << " + " << force_up);
        DEBUGIO << "\tF = Fs + Fdown + Fup = " << F << "\n";
    } else {
        F = force_down + force_up;
        LOG4CXX_TRACE(logger, " -> force(2) is " << F << " = " << force_down << " + " << force_up);
        DEBUGIO << "\tF = Fdown + Fup = " << F << "\n";
    }

    if (F != F) {
        LOG4CXX_TRACE(logger, " -> force is not a number!");
        reset();
        return;
    } else if (abs(F) < conf->sensitivity_threshold_) {
        LOG4CXX_TRACE(logger, " -> force below sensitivity threshold " << conf->sensitivity_threshold_ << "!");
        reset();
        return;
    } else if (abs(F) > conf->force_limit_) {
        LOG4CXX_TRACE(logger, " -> force limit " << conf->force_limit_ << " exceeded!");
        if (conf->enforce_limits_) {
            F = (F < 0.0 ? -conf->force_limit_ : conf->force_limit_);
            DEBUGIO << "\tF = " << F << "\n";
        }
    }

    double T = temperature(F);
    LOG4CXX_TRACE(logger, " -> temperature is " << T);
    DEBUGIO << "\tT = " << T << "\n";

    adjust(F, T);
    LOG4CXX_TRACE(logger, " => adjusting leader height by " << change);
    DEBUGIO << "\tc = " << change << "\n";

    if (stat.enabled) {
        stat.total_spring_force += abs(Fs);
        stat.total_repulsive_force += -force_down + force_up;
        stat.total_force += abs(force);
        stat.total_temperature += temp;
        stat.total_change += abs(change);
    }
}

double SpringMatchingLF::repulsion_force(const ScreenSpace& s) const
{
    if (space.xintersects(s)) {
        double Fs = abs(spring_force(height));
        if (space.yintersects(s)) {
            double o = 1.0 - space.overlap(s)/static_cast<double>(space.area());
            double f = 1.0/(o * o) - (1.0 - Fs);
            assert(f == f);
            DEBUGIO << "\t\t\t1 - o/h = " << space.overlap(s) << " / " << space.area() << " = " << o << "\n"
                    << "\t\t\tf = " << f << "\n";
            return space.ysign(s) * (f > conf->force_limit_ ? conf->force_limit_ : f);
        }

        double dy = double(space.yoffset(s)) / double(space.height());
        if (dy < 1.0) {
            double d = 1.0 - dy;
            // This gives us a nice quadratic curve where the repulsion force is equal to
            // the spring force when they are right next to each other, i.e. dy = 0.
            return space.ysign(s) * Fs * d*d;
        }
    }
    return 0.0;
}

std::pair<double, double> SpringMatchingLF::repulsion_forces2(const list<LabelForce*>& labels) const
{
    double Fdown = 0.0;
    double Fup = 0.0;

    for (const LabelForce* alt : labels) {
        double F = repulsion_force(alt->getScreenSpace());
        DEBUGIO << "\t\tF <= " << F << " :: " << alt->getLabel()->getName() << "\n";
        if (F < 0.0) {
            Fdown += F;
        } else {
            Fup += F;
        }
    }

    DEBUGIO << "\t\tFdown * Fr_const = " << Fdown << " * " << conf->repulsion_constant_ << "\n";
    DEBUGIO << "\t\tFup * Fr_const = " << Fup << " * " << conf->repulsion_constant_ << "\n";
    return make_pair(conf->repulsion_constant_ * Fdown, conf->repulsion_constant_ * Fup);
}

} // namespace asl
