/**
 * \file spring_matching_fd.h
 *
 * \author Ben Morgan <benjamin.morgan@stud-mail.uni-wuerzburg.de>
 * \date   04. November 2014
 */

#ifndef INCLUDE_GUARD_E0330DF2168D4C8AA65099CE1C893BF9
#define INCLUDE_GUARD_E0330DF2168D4C8AA65099CE1C893BF9

#include "linear_fd.h"

#include <utility>
#include <list>

namespace asl {

class SpringMatchingFDA;

/**
 * SpringMatchingLF tries to adjust the repulsion force exerted by label spaces
 * so that it better correlates to the spring force of the label. This helps
 * achieve an equilibrium faster and prevents many labels from pushing individual
 * labels around too much. If a label is more than it's own height away from
 * another label, then the other label has no effect on it.
 *
 * In this scheme of things, conf->force_base_ is not needed.
 */
class SpringMatchingLF : public LinearLF {
public:
    SpringMatchingLF(SpringMatchingFDA* fda, Label* l);
    ~SpringMatchingLF() {}

    std::string str() const;
    void animate(const std::list<LabelForce*>& labels);
    double repulsion_force(const ScreenSpace& s) const;

    /// repulsion_forces2 returns the down forces separately from the up forces.
    std::pair<double, double> repulsion_forces2(const std::list<LabelForce*>& labels) const;

protected:
    double force_down;
    double force_up;
};

/// SpringMatchingFDA uses SpringMatchingLF to calculate label forces.
class SpringMatchingFDA : public LinearFDA {
public:
    friend SpringMatchingLF;

    SpringMatchingFDA() {}
    ~SpringMatchingFDA() {}

    static void options(boost::program_options::options_description&) {}

protected:
    LabelForce* new_labelforce(Label* lab) {
        return new SpringMatchingLF(this, lab);
    }
};

} // namespace asl
#endif // INCLUDE_GUARD
