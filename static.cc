/*
 * static.cc
 *
 * Ben Morgan <benjamin.morgan@stud-mail.uni-wuerzburg.de>
 * 19. December 2014
 */

#include "static.h"
#include "statistics.h"
#include "util/screenspace.h"
#include "util/svg.h"
#include "util/timer.h"

#include <cassert>
#include <string>
using namespace std;

#include <straph/point.h>
#include <straph/straph.h>
using namespace straph;

#include <osg/ref_ptr>
#include <osg/Group>
using namespace osg;

#include <log4cxx/logger.h>

// ALGO_NS is the configuration namespace for this class.
#define ALGO_NS "algorithm.static."

namespace asl {

namespace {

log4cxx::LoggerPtr logger = log4cxx::Logger::getLogger("asl.algorithm.static");

Point path_middle(const RouteSegment& rs)
{
    Polyline poly;
    for (const Segment& s : rs.segments) {
        poly.insert(poly.end(), s.polyline.begin(), s.polyline.end());
    }
    return path_middle(poly);
}

inline Point path_middle(const Segment& rs)
{
    return path_middle(rs.polyline);
}

}

void StaticAlgorithm::options(boost::program_options::options_description& opt)
{
    namespace po = boost::program_options;

    opt.add_options()
        (ALGO_NS "leader_height"         , po::value<double>()->default_value(10.0), "Default height of the label leader.")
        (ALGO_NS "max_active_labels"     , po::value<unsigned>()->default_value(10), "Maximum number of labels placed on the scene at a time.")
        (ALGO_NS "min_available_labels"  , po::value<unsigned>()->default_value(0) , "Minimum number of available labels, that is the number of active labels + the number of unborn labels. Quit when the available labels < min_available_labels. Disabled if 0.")
        (ALGO_NS "per_segment_labels"    , po::value<bool>()->default_value(false) , "Allow duplicate labels per segment (currently not implemented).")
        (ALGO_NS "pointer_privacy_sphere", po::value<double>()->default_value(10.0), "Remove the first label in the queue when it's distance to pointer is less than this.")
        (ALGO_NS "insignificance_threshold", po::value<unsigned>()->default_value(100), "Minimum area a label must occupy to not be ignored.")
        ;
}

void StaticAlgorithm::configure(const boost::program_options::variables_map& vm)
{
    Algorithm::configure(vm);

    leader_height_          = vm[ALGO_NS "leader_height"].as<double>();
    max_active_labels_      = vm[ALGO_NS "max_active_labels"].as<unsigned>();
    min_available_labels_       = vm[ALGO_NS "min_available_labels"].as<unsigned>();
    per_segment_labels_     = vm[ALGO_NS "per_segment_labels"].as<bool>();
    pointer_privacy_sphere_ = vm[ALGO_NS "pointer_privacy_sphere"].as<double>();
    insig_threshold_ = vm[ALGO_NS "insignificance_threshold"].as<unsigned>();

    assert(leader_height_ > 0.0);
    assert(pointer_privacy_sphere_ > 0.0);
    assert(insig_threshold_ >= 0.0);
}

bool StaticAlgorithm::setup_hook(osg::Group* scene, const straph::Route& route, const Voyager&)
{
    LOG4CXX_INFO(logger, "Setting up static labeling...");

    if (route.empty()) {
        LOG4CXX_ERROR(logger, "Route is empty!");
        return false;
    } else if (route.size() < min_available_labels_) {
        LOG4CXX_ERROR(logger, "Route size is less than min_available_labels!");
        return false;
    }

    // This is why we use a label factory!
    LabelFactory lf(label_font_path_, label_font_size_, label_font_color_,
            leader_height_, label_draw_box_, label_fill_box_, label_box_margin_,
            label_box_background_color_, label_box_border_color_);

    // Create all the labels and add them to the group and the vector.
    // We don't bother using ref_ptr, because all the labels are in the
    // scene graph; and it's anyway just really annoying.
    Group* g = new Group;
    for (size_t i = 0; i < route.size(); i++) {
        LOG4CXX_TRACE(logger, "Creating label " << route[i].name);
        Label* k = lf.create(path_middle(route[i]), route[i].name);

        if (i < max_active_labels_ || max_active_labels_ == 0) {
            active_labels_.push_back(k);
        } else {
            k->hide();
            unborn_labels_.push_back(k);
        }
        g->addChild(k);
    }
    scene->addChild(g);

    // Set the nearest label position (when we get close, we hide the label).
    nearest_label_pos_ = active_labels_.front()->getPoint();

    return true;
}

/*
 * Here we hide labels if they come close to being passed by the pointer, and we maintain
 * the maximum number of visible labels that are to be shown. When we hide a label, we show
 * another one, etc.
 */
bool StaticAlgorithm::update_hook(osg::Group*, const osgViewer::Viewer& viewer, const Voyager& voyager)
{
    // If there are no visible labels, then there is nothing to do.
    bool ok = true;
    if (active_labels_.empty()) {
        return true;
    }

    // If the privacy sphere is too small, then none of the labels may ever be hidden due
    // to pointer movement, etc. Therefore, we assure that the pointer_privacy_sphere_ is
    // larger than the current voyager travel speed.
    if (pointer_privacy_sphere_ < voyager.getSpeed()) {
        // TODO: remove this magic number.
        pointer_privacy_sphere_ = voyager.getSpeed() * 1.5;
        LOG4CXX_WARN(logger, " -> Increasing pointer privacy sphere to " << pointer_privacy_sphere_);
    }

    // Check if a label needs to be hidden.
    Point cur_pos = voyager.getRoute()->position();
    if (cur_pos.distance(nearest_label_pos_) < pointer_privacy_sphere_) {
        Label* f = active_labels_.front();

        // If we are not on the segment that the label is on, then we don't hide it yet.
        if (voyager.getRoute()->segment()->name != f->getName()) {
            goto skip_hide;
        }

        // Hide the front-most label
        LOG4CXX_INFO(logger, "Reached label " << f->getName());
        if (stat.enabled && !leader_stats_.empty()) {
            leader_stats_.pop_front();
        }
        active_labels_.pop_front();
        hide_label_hook(f);

        // Add the next label to the list of visible labels and show it
        if (!unborn_labels_.empty()) {
            Label* n = unborn_labels_.front();
            if (stat.enabled && leader_stats_.size() == active_labels_.size()) {
                Translator t(viewer.getCamera());
                leader_stats_.push_back(t.leader(n));
            }
            active_labels_.push_back(n);
            unborn_labels_.pop_front();
            show_label_hook(n);
        }

        // TODO: unborn_labels.size() + active_labels.size() can be optimized, right?
        if (min_available_labels_ > 0 && unborn_labels_.size() + active_labels_.size() < min_available_labels_) {
            LOG4CXX_INFO(logger, "Quitting, because available labels < min_available_labels.");
            ok = false;
        }

        // Update next nearest point
        if (!active_labels_.empty()) {
            nearest_label_pos_ = active_labels_.front()->getPoint();
        }
    }
skip_hide:

    // Statistics:
    if (stat.enabled) {
        Timer statistics_timer(&stat.total_time_statistics, true);

        // Update the statistics; this code based directly off force_directed.cc
        Translator t(viewer.getCamera());
        ScreenSpace screen = t.screen();
        auto quarter_screen_area = screen.area() / 16;

        // Stat: number of active labels
        stat.total_active_labels += active_labels_.size();

        // During setup, we did not have access to the camera,
        // so we need to catch up here.
        if (leader_stats_.empty() && !active_labels_.empty()) {
            for (Label* label : active_labels_) {
                leader_stats_.push_back(t.leader(label));
            }
        }

        assert(active_labels_.size() == leader_stats_.size());
        list<ScreenSpace> visible_labels;
        auto alit = active_labels_.begin();
        auto lsit = leader_stats_.begin();
        for (; alit != active_labels_.end(); alit++, lsit++) {
            Label* label = *alit;
            ScreenSpace space = t.screenspace(label);

            // If a label overlaps the entire screen, then it's an error.
            if (space.behind() ||
                space.area() > quarter_screen_area ||
                space.area() < insig_threshold_ ||
                !space.intersects(screen)) {
                continue;
            }
            visible_labels.push_back(space);

            // Stat: leader heights
            double h = label->getHeight();
            if (h < stat.min_leader_height) {
                stat.min_leader_height = h;
            }
            if (h > stat.max_leader_height) {
                stat.max_leader_height = h;
            }
            stat.total_leader_height += h;
            stat.total_leader_height_variance += abs(h - leader_height_);

            // Stat: screen-space leader heights
            Leader next = t.leader(label);
            double nexth = next.length();
            stat.total_leader_ss_height += nexth;
            if (nexth < stat.min_leader_ss_height) {
                stat.min_leader_ss_height = nexth;
            }
            if (nexth > stat.max_leader_ss_height) {
                stat.max_leader_ss_height = nexth;
            }
            Vec3 wanted = next.world;
            wanted.z() = leader_height_;
            stat.total_leader_ss_height_variance += next.top.distance(t.point(wanted));
            if (lsit->world != next.world) {
                stat.total_leader_changes++;
                // We calculate what the last leader top would be with the current camera,
                // and calculate the difference with the current leader top.
                stat.total_leader_ss_change += t.point(lsit->world).distance(next.top);
            }
            *lsit = next;
        }

        // Stat: number of visible labels
        stat.total_visible_labels += visible_labels.size();

        // Stat: frame and total pixel overlap
        // We count the overlap only once by popping a label and not inserting it again.
        unsigned pixel_overlap = 0;
        unsigned ypixel_overlap = 0;
        while (!visible_labels.empty()) {
            ScreenSpace space = visible_labels.front();
            visible_labels.pop_front();

            for (ScreenSpace& other : visible_labels) {
                if (space.intersects(other)) {
                    ypixel_overlap += space.yoverlap(other);
                    pixel_overlap += space.overlap(other);
                }
            }
        }
        if (pixel_overlap > quarter_screen_area) {
            LOG4CXX_WARN(logger, "Pixel overlap for frame " << stat.frame_pixel_overlaps.size() << " too large: " << pixel_overlap);
        }
        stat.frame_pixel_overlaps.push_back(pixel_overlap);
        stat.total_pixel_overlap += pixel_overlap;
        stat.total_visible_yoverlap += ypixel_overlap;
    }

    return ok;
}

void StaticAlgorithm::svg_hook(osg::Group*, osgViewer::Viewer& viewer, const std::string filepath)
{
    Translator t(viewer.getCamera());
    ScreenSpace screen = t.screen();

    SVGWriter w(filepath, screen);
#ifdef ENABLE_IMAGE_DEBUGGING
    w << svg::background(viewer);
#endif
    for (const Label* lab : active_labels_) {
        ScreenSpace s = t.screenspace(lab);
        if (s.intersects(screen)) {
            w << svg::Rect(s);
        }
    }
}

void StaticAlgorithm::show_label_hook(Label* a)
{
    LOG4CXX_DEBUG(logger, "Showing label " << a->getName());
    a->show();
}

void StaticAlgorithm::hide_label_hook(Label* a)
{
    LOG4CXX_DEBUG(logger, "Hiding label " << a->getName());
    a->hide();
}

} // namespace asl
