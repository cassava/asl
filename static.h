/**
 * \file static.h
 *
 * \author Ben Morgan <benjamin.morgan@stud-mail.uni-wuerzburg.de>
 * \date   19. December 2014
 */

#ifndef INCLUDE_GUARD_B1D9F239AA3148E7B90BF0ADAED8F6EF
#define INCLUDE_GUARD_B1D9F239AA3148E7B90BF0ADAED8F6EF

#include "asl.h"
#include "util/color.h"
#include "util/draw.h"
#include "util/screenspace.h"

#include <string>
#include <vector>

#include <straph/point.h>

#include <osg/Group>

namespace asl {

class StaticAlgorithm : public Algorithm {
public:
    StaticAlgorithm() {}
    virtual ~StaticAlgorithm() {}

    static void options(boost::program_options::options_description& opt);
    virtual void configure(const boost::program_options::variables_map& config);

protected:
    virtual bool setup_hook(osg::Group* scene, const straph::Route& route, const Voyager& voyager);
    virtual bool update_hook(osg::Group* scene, const osgViewer::Viewer& viewer, const Voyager& voyager);
    virtual void svg_hook(osg::Group* scene, osgViewer::Viewer& viewer, const std::string path);

    // New interface for following algorithms! :-)
    virtual void show_label_hook(Label* label);
    virtual void hide_label_hook(Label* label);

protected:
// data members:
    double   leader_height_;
    unsigned max_active_labels_;
    unsigned min_available_labels_;
    bool     per_segment_labels_;
    double   pointer_privacy_sphere_;
    unsigned insig_threshold_;

    std::list<Label*> unborn_labels_;
    std::list<Label*> active_labels_;
    std::list<Leader> leader_stats_;

    straph::Point nearest_label_pos_;
};

} // namespace asl
#endif // INCLUDE_GUARD
