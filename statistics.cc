/*
 * statistics.cc
 *
 * Ben Morgan <benjamin.morgan@stud-mail.uni-wuerzburg.de>
 * 19. December 2014
 */

#include "statistics.h"
#include "util/filewriter.h"

#include <iostream>
#include <string>
#include <sstream>
using namespace std;

#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/filesystem.hpp>
using namespace boost::filesystem;

#include <log4cxx/logger.h>

// ALGO_NS is the configuration namespace for this class.
#define ALGO_NS "statistics."

namespace asl {

namespace {

log4cxx::LoggerPtr logger = log4cxx::Logger::getLogger("asl.statistics");

// TODO: Convert this into a generic function!
// Also, it would be useful if we knew if it worked!
std::string vec2str(const vector<size_t>& lst)
{
    if (lst.empty()) {
        return "[]";
    }

    ostringstream lststr;
    lststr << "[";
    for (size_t i = 0; i < lst.size()-1; i++) {
        lststr << lst[i] << " ";
    }
    lststr << lst[lst.size()-1] << "]";
    return lststr.str();
}

} // anonymous namespace

Statistics stat{};

void Statistics::options(boost::program_options::options_description& opt)
{
    namespace po = boost::program_options;

    opt.add_options()
        (ALGO_NS "enabled"          , po::value<bool>()->default_value(true)                      , "Track algorithm more comprehensively for better statistics and report them. If true, filepath must be set.")
        (ALGO_NS "filepath"         , po::value<string>()->default_value("statistics.csv")        , "The comprehensive statistics report is written to this file. Data is appended to this file.")
        (ALGO_NS "filepath_overlap" , po::value<string>()->default_value("statistics_overlap.dat"), "If not empty, pixel overlap for each frame is written to this file. Existing file will be truncated.")
        (ALGO_NS "delimiter"        , po::value<char>()->default_value(',')                       , "CSV delimiter for statistics report.")
        ;
}

void Statistics::configure(const boost::program_options::variables_map& vm)
{
    config_ = &vm;

    enabled           = vm[ALGO_NS "enabled"].as<bool>();
    filepath_         = vm[ALGO_NS "filepath"].as<string>();
    filepath_overlap_ = vm[ALGO_NS "filepath_overlap"].as<string>();
    delimiter_        = vm[ALGO_NS "delimiter"].as<char>();
}

void Statistics::report_log()
{
    double total_s = static_cast<double>(total_time.total_milliseconds()) / 1.0e3;
    double total_s_rotating = static_cast<double>(total_time_rotating.total_milliseconds()) / 1.0e3;
    double total_s_debugging = static_cast<double>(total_time_debugging.total_milliseconds()) / 1.0e3;
    double total_s_statistics = static_cast<double>(total_time_statistics.total_milliseconds()) / 1.0e3;
    double total_s_algorithm = static_cast<double>(total_time_algorithm.total_milliseconds() - total_time_statistics.total_milliseconds()) / 1.0e3;
    double total_ms_algorithm = static_cast<double>(total_time_algorithm.total_nanoseconds() - total_time_statistics.total_nanoseconds()) / 1.0e6;
    LOG4CXX_INFO(logger, "Stat: Route length = " << route_length);
    LOG4CXX_INFO(logger, "Stat: Seconds = " << total_s);
    LOG4CXX_INFO(logger, "Stat: Seconds rotating = " << total_s_rotating);
    LOG4CXX_INFO(logger, "Stat: Seconds debugging = " << total_s_debugging);
    LOG4CXX_INFO(logger, "Stat: Seconds statistics = " << total_s_statistics);
    LOG4CXX_INFO(logger, "Stat: Seconds algorithm = " << total_s_algorithm);
    LOG4CXX_INFO(logger, "Stat: Frames = " << total_frames);
    LOG4CXX_INFO(logger, "Stat: Algorithm ms/frame = " << total_ms_algorithm/total_frames);
    LOG4CXX_INFO(logger, "Stat: Mean frames/second = " << total_frames/total_s);

    // A little bit of statistics is ok. :-)
    // But the rest of the statistics are only recorded if we have enabled them,
    // so we stop at this point.
    if (!enabled) {
        return;
    }

    double total_visible_labels_d = static_cast<double>(total_visible_labels);
    double total_directed_labels = static_cast<double>(total_active_labels - total_ignored);
    double total_frames_d = static_cast<double>(total_frames);
    LOG4CXX_INFO(logger, "Stat: Mean active-labels/frame = " << total_active_labels/total_frames_d);
    LOG4CXX_INFO(logger, "Stat: Mean visible-labels/frame = " << total_visible_labels/total_frames_d);
    LOG4CXX_INFO(logger, "Stat: Total pixel-overlap = " << total_pixel_overlap);
    LOG4CXX_INFO(logger, "Stat: Mean pixel-overlap/frame = " << total_pixel_overlap/total_frames);

    LOG4CXX_INFO(logger, "Stat: Total visible yoverlap = " << total_visible_yoverlap);
    LOG4CXX_INFO(logger, "Stat: Total visible yoverlap/frame = " << total_visible_yoverlap/total_frames_d);
    LOG4CXX_INFO(logger, "Stat: Mean yoverlap per visible label/frame = " << total_visible_yoverlap/total_visible_labels_d);

    LOG4CXX_INFO(logger, "NOTE: All of the leader height statistic concerns visible labels.");
    LOG4CXX_INFO(logger, "Stat: Min leader height = " << min_leader_height);
    LOG4CXX_INFO(logger, "Stat: Max leader height = " << max_leader_height);
    LOG4CXX_INFO(logger, "Stat: Mean leader-height = " << total_leader_height/total_visible_labels_d);
    LOG4CXX_INFO(logger, "Stat: Mean leader-height variance = " << total_leader_height_variance/total_visible_labels_d);

    LOG4CXX_INFO(logger, "Stat: Min leader screenspace height = " << min_leader_ss_height);
    LOG4CXX_INFO(logger, "Stat: Max leader screenspace height = " << max_leader_ss_height);
    LOG4CXX_INFO(logger, "Stat: Mean leader screenspace height = " << total_leader_ss_height/total_visible_labels_d);
    LOG4CXX_INFO(logger, "Stat: Mean leader screenspace height variance = " << total_leader_ss_height_variance/total_visible_labels_d);
    LOG4CXX_INFO(logger, "Stat: Mean leader screenspace height change = " << total_leader_ss_change/total_leader_changes);

    LOG4CXX_INFO(logger, "Stat: Mean net spring-force/label = " << total_spring_force/total_directed_labels);
    LOG4CXX_INFO(logger, "Stat: Mean net repulsive-force/label = " << total_repulsive_force/total_directed_labels);
    LOG4CXX_INFO(logger, "Stat: Mean net forces/label = " << total_force/total_directed_labels);
    LOG4CXX_INFO(logger, "Stat: Mean temperature/label = " << total_temperature/total_directed_labels);
    LOG4CXX_INFO(logger, "Stat: Mean net change/label = " << total_change/total_directed_labels);
    LOG4CXX_INFO(logger, "Stat: Mean ignored labels/frame = " << total_ignored/total_frames_d);
}

bool Statistics::report_csv(const std::string& path)
{
    bool creat = !exists(path);
    FileWriter of(path, false);
    if (!of) {
        LOG4CXX_ERROR(logger, "Could not open file " << path << " for writing!");
        return false;
    }

    // Some needed conversions:
    double total_frames_d = static_cast<double>(total_frames);
    double total_s = static_cast<double>(total_time.total_milliseconds()) / 1.0e3;
    double total_s_rotating = static_cast<double>(total_time_rotating.total_milliseconds()) / 1.0e3;
    double total_s_debugging = static_cast<double>(total_time_debugging.total_milliseconds()) / 1.0e3;
    double total_s_statistics = static_cast<double>(total_time_statistics.total_milliseconds()) / 1.0e3;
    double total_s_algorithm = static_cast<double>(total_time_algorithm.total_milliseconds() - total_time_statistics.total_milliseconds()) / 1.0e3;
    double total_ms_algorithm = static_cast<double>(total_time_algorithm.total_nanoseconds() - total_time_statistics.total_nanoseconds()) / 1.0e6;
    double total_visible_labels_d = static_cast<double>(total_visible_labels);
    double total_directed_labels = static_cast<double>(total_active_labels - total_ignored);
    const boost::program_options::variables_map& vm = *config_;

    ostringstream keys, values;
    #define entry(x, y) keys << x << delimiter_, values << y << delimiter_
    entry("config", vm["config"].as<string>());
    entry("algo", vm["asl.algorithm"].as<string>());
    if (vm.count("straph.route")) {
        auto r = vm["straph.route"].as<vector<size_t>>();
        entry("route", vec2str(r));
    } else {
        entry("route", "unknown");
    }
    entry("only_route", vm["asl.show_only_route"].as<bool>());

    entry("st.leader_height", vm["algorithm.static.leader_height"].as<double>());
    entry("st.max_active_labels", vm["algorithm.static.max_active_labels"].as<unsigned>());
    // TODO:
    //entry("st.min_available_labels", vm["algorithm.static.min_available_labels"].as<unsigned>());
    entry("fd.label_margin", vm["algorithm.force_directed.label_margin_bottom"].as<int>());
    entry("fd.spring_constant", vm["algorithm.force_directed.spring_constant"].as<double>());
    entry("fd.rep_constant", vm["algorithm.force_directed.repulsion_constant"].as<double>());
    entry("fd.beta_constant", vm["algorithm.force_directed.beta_constant"].as<double>());
    entry("fd.force_limit", vm["algorithm.force_directed.force_limit"].as<double>());
    entry("fd.temp_base", vm["algorithm.force_directed.temperature_base"].as<double>());
    entry("fd.temp_step", vm["algorithm.force_directed.temperature_step"].as<double>());
    entry("fd.temp_limit", vm["algorithm.force_directed.temperature_limit"].as<double>());
    entry("fd.enforce_limits", vm["algorithm.force_directed.enforce_limits"].as<bool>());
    entry("fd.sensi_thresh", vm["algorithm.force_directed.sensitivity_threshold"].as<double>());
    // TODO: rename this to st.insig_thresh or something
    entry("fd.insig_thresh", vm["algorithm.static.insignificance_threshold"].as<unsigned>());

    entry("total_s", total_s);
    entry("total_s_rotating", total_s_rotating);
    entry("total_s_debugging", total_s_debugging);
    entry("total_s_statistics", total_s_statistics);
    entry("total_s_algorithm", total_s_algorithm);
    entry("total_ms_algorithm", total_ms_algorithm);
    entry("total_frames", total_frames);
    entry("mean_algo_ms_per_frame", total_ms_algorithm/total_frames_d);
    entry("mean_frames_per_second", total_frames/total_s);

    entry("mean_active_labels_per_frame", total_active_labels/total_frames_d);
    entry("mean_pixel_overlap_per_frame", total_pixel_overlap/total_frames);
    entry("mean_pixel_overlap_per_visible_label", total_pixel_overlap/total_visible_labels_d);
    entry("min_leader_height", min_leader_height);
    entry("max_leader_height", max_leader_height);

    entry("mean_leader_height", total_leader_height/total_visible_labels_d);
    entry("mean_leader_height_variance", total_leader_height_variance/total_visible_labels_d);

    entry("mean_net_spring_force_per_label", total_spring_force/total_directed_labels);
    entry("mean_net_repulsive_force_per_label", total_repulsive_force/total_directed_labels);
    entry("mean_net_forces_per_label", total_force/total_directed_labels);
    entry("mean_temperature_per_label", total_temperature/total_directed_labels);
    entry("mean_net_change_per_label", total_change/total_directed_labels);
    entry("total_route_length", route_length);
    entry("total_ignored_labels", total_ignored);
    entry("mean_ignored_labels_per_frame", total_ignored/total_frames_d);
    entry("mean_visible_labels_per_frame", total_visible_labels_d/total_frames_d);
    entry("total_pixel_overlap", total_pixel_overlap);

    // This is at the end so it doesn't bother Nadine.
    entry("st.min_available_labels", vm["algorithm.static.min_available_labels"].as<unsigned>());

    entry("min_leader_ss_height", min_leader_ss_height);
    entry("max_leader_ss_height", max_leader_ss_height);
    entry("mean_leader_ss_height", total_leader_ss_height/total_visible_labels_d);
    entry("mean_leader_ss_height_variance", total_leader_ss_height_variance/total_visible_labels_d);
    entry("mean_leader_ss_height_change", total_leader_ss_change/total_leader_changes);

    entry("total_visible_yoverlap", total_visible_yoverlap);
    entry("mean_visible_yoverlap_per_frame", total_visible_yoverlap/total_frames_d);
    entry("mean_visible_yoverlap_per_label_per_frame", total_visible_yoverlap/total_visible_labels_d);

    #undef entry

    if (creat) {
        of << keys.str() << endl;
    }
    of << values.str() << endl;
    of.close();

    // Write frame pixel overlap values, if filepath_overlap_ not empty.
    // Overwrite existing files.
    if (!filepath_overlap_.empty()) {
        FileWriter ovlp_of(filepath_overlap_, true);
        if (!ovlp_of) {
            LOG4CXX_ERROR(logger, "Could not open file " << path << " for writing!");
            return false;
        }

        for (unsigned i : frame_pixel_overlaps) {
            ovlp_of << i << '\n';
        }
        ovlp_of.close();
    }

    return true;
}

void Statistics::report()
{
    report_log();
    if (enabled && !filepath_.empty()) {
        report_csv(filepath_);
    }
}

} // namespace asl
