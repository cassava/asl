/**
 * \file statistics.h
 *
 * \author Ben Morgan <benjamin.morgan@stud-mail.uni-wuerzburg.de>
 * \date   19. December 2014
 */

#ifndef INCLUDE_GUARD_784CF6179C274D77ABE1802D1C456613
#define INCLUDE_GUARD_784CF6179C274D77ABE1802D1C456613

#include <string>
#include <limits>

#include <boost/date_time/posix_time/posix_time.hpp>

// Forward declaration optimization
namespace boost {
    namespace program_options {
        // #include <boost/program_options/options_description.hpp>
        class options_description;
        // #include <boost/program_options/variables_map.hpp>
        class variables_map;
    }
}

namespace asl {

struct Statistics {
    Statistics()
        : enabled(false)
        , route_length(0)
        , total_frames(0)
        , total_active_labels(0)
        , total_visible_labels(0)
        , total_pixel_overlap(0)
        , total_ignored(0)
        , min_leader_height(std::numeric_limits<double>::infinity())
        , max_leader_height(0.0)
        , total_leader_height(0.0)
        , total_leader_height_variance(0.0)
        , min_leader_ss_height(std::numeric_limits<double>::infinity())
        , max_leader_ss_height(0.0)
        , total_leader_ss_height(0.0)
        , total_leader_ss_height_variance(0.0)
        , total_leader_changes(0)
        , total_leader_ss_change(0.0)
        , total_visible_yoverlap(0.0)
        , total_spring_force(0.0)
        , total_repulsive_force(0.0)
        , total_force(0.0)
        , total_temperature(0.0)
        , total_change(0.0)
        , config_(nullptr)
        , filepath_("statistics.csv")
        , delimiter_(',')
    {}

    static void options(boost::program_options::options_description& opt);
    void configure(const boost::program_options::variables_map& config);

    void report_log();
    bool report_csv(const std::string& filename);
    void report();

public:
    bool enabled;
    unsigned long long route_length;
    unsigned long long total_frames;

    boost::posix_time::time_duration total_time;
    boost::posix_time::time_duration total_time_rotating;
    boost::posix_time::time_duration total_time_debugging;
    boost::posix_time::time_duration total_time_statistics;
    boost::posix_time::time_duration total_time_algorithm;
    unsigned long long total_active_labels;
    unsigned long long total_visible_labels;
    unsigned long long total_pixel_overlap;
    unsigned long long total_ignored;
    std::vector<unsigned> frame_pixel_overlaps;

    double min_leader_height;
    double max_leader_height;
    double total_leader_height;
    double total_leader_height_variance;

    double min_leader_ss_height;
    double max_leader_ss_height;
    double total_leader_ss_height;
    double total_leader_ss_height_variance;
    unsigned long long total_leader_changes;
    double total_leader_ss_change;

    double total_visible_yoverlap;

    double total_spring_force;
    double total_repulsive_force;
    double total_force;
    double total_temperature;
    double total_change;

private:
    const boost::program_options::variables_map* config_;
    std::string filepath_;
    std::string filepath_overlap_;
    char delimiter_;
};

extern Statistics stat;

} // namespace asl
#endif // INCLUDE_GUARD
