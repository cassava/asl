/*
 * main.cc
 *
 * Command tourable reads in a shape file using libstraph and displays
 * it using libasl.
 *
 * Ben Morgan <benjamin.morgan@stud-mail.uni-wuerzburg.de>
 * 05. January 2015
 */

#include "asl.h"
#include "static.h"
#include "force_directed.h"
#include "linear_fd.h"
#include "quadratic_fd.h"
#include "exponential_fd.h"
#include "spring_matching_fd.h"
using namespace asl;

#include <straph/straph.h>
#include <straph/routing.h>
using namespace straph;

#include <fstream>
#include <iostream>
#include <memory>
#include <utility>
#include <vector>
using namespace std;

#include <boost/program_options.hpp>
namespace po = boost::program_options;

#include <log4cxx/logger.h>
#include <log4cxx/helpers/transcoder.h>
#include <log4cxx/basicconfigurator.h>
#include <log4cxx/consoleappender.h>
#include <log4cxx/propertyconfigurator.h>
#include <log4cxx/patternlayout.h>

// Forward declarations:
po::variables_map process_options(int argc, char** argv);

log4cxx::LoggerPtr logger{log4cxx::Logger::getLogger("tourable")};

/*
 * Maps a string key to a RouteGenerator and returns nullptr on failure.
 */
unique_ptr<RouteGenerator> get_route_generator(string key)
{
    if (key == "bfs") {
        return unique_ptr<RouteGenerator>(new BFSRouter);
    } else if (key == "dfs") {
        return unique_ptr<RouteGenerator>(new DFSRouter);
    } else if (key == "noop" || key == "disable" || key == "disabled") {
        return unique_ptr<RouteGenerator>(new NoopRouteGenerator);
    }
    return nullptr;
}

/*
 * Maps a string key to an Algorithm and returns nullptr on failure.
 *
 * If you want to add a new Algorithm, make sure you add it to the backend
 * options. (Search for tag_backend_options.)
 */
unique_ptr<Algorithm> get_algorithm(string key)
{
    if (key == "base") {
        return unique_ptr<Algorithm>(new Algorithm);
    } else if (key == "static") {
        return unique_ptr<Algorithm>(new StaticAlgorithm);
    } else if (key == "linear_fd") {
        return unique_ptr<Algorithm>(new LinearFDA);
    } else if (key == "quadratic_fd") {
        return unique_ptr<Algorithm>(new QuadraticFDA);
    } else if (key == "exponential_fd") {
        return unique_ptr<Algorithm>(new ExponentialFDA);
    } else if (key == "spring_matching_fd") {
        return unique_ptr<Algorithm>(new SpringMatchingFDA);
    }
    return nullptr;
}

int main(int argc, char** argv)
{
    // 1. Process the options from the commandline interface and the
    //    configuration file(s) and return a variables map.
    po::variables_map vm = process_options(argc, argv);

    unique_ptr<Straph> sg;
    unique_ptr<Route> rt;

    // 2. Short-circuit all the following logic if we have a test route already.
    if (vm.count("test.input_file")) {
        auto file = vm["test.input_file"].as<string>();
        auto scale = vm["test.scale"].as<double>();
        rt = read_route(file, scale);
        if (!rt) {
            LOG4CXX_ERROR(logger, "Unable to read route. Exiting...");
            return EXIT_FAILURE;
        }
        // And skip to stage 5...
    } else {
        // 3. Read the shapefile into a street graph and scale it if necessary.
        auto file = vm["straph.input_file"].as<string>();
        auto name_idx = vm["straph.name_index"].as<unsigned>();
        auto normalize = vm["straph.normalize"].as<bool>();
        auto rename = vm["straph.rename"].as<bool>();
        auto scale = vm["straph.scale"].as<double>();
        auto print = vm["straph.print_route"].as<bool>();
        sg = read_shapefile(file, name_idx);
        if (!sg) {
            LOG4CXX_ERROR(logger, "Street graph is empty! Exiting...");
            return EXIT_FAILURE;
        }
        if (normalize) {
            normalize_straph(*sg);
        }
        if (scale != 1.0) {
            scale_straph(*sg, scale);
        }

        // 4. Find out which routing algorithm should be used and fail if it doesn't exit.
        string router_key = vm["straph.algorithm"].as<string>();
        unique_ptr<RouteGenerator> router = get_route_generator(router_key);
        if (!router) {
            LOG4CXX_ERROR(logger, "Routing algorithm " << router_key << " is unknown. Exiting...");
            return EXIT_FAILURE;
        }
        router->initialize(*sg);
        vector<size_t> route_path;
        if (vm.count("straph.route")) {
            route_path = vm["straph.route"].as<vector<size_t>>();
        }
        rt = router->generate_route(route_path);
        if (rt == nullptr) {
            if (vm["straph.require_route"].as<bool>()) {
                return EXIT_FAILURE;
            }
        }
        if (rename) {
            route_rename(*rt);
        }
        if (print) {
            vector<straph::Junction> js = straph::get_route_junctions(*sg, *rt);
            cout << "ROUTE JUNCTION INDICES:" << endl;
            cout << "------------------------------------------" << endl;
            cout << "tourable";
            for (straph::Junction j : js) {
                cout << " -r" << j.index;
            }
            cout << endl;
            cout << "------------------------------------------" << endl;
            for (straph::Junction j : js) {
                cout << "route = " << j.index << endl;
            }
            cout << "------------------------------------------" << endl;
        }
    }

    // 5. Get the algorithm, configure it, and run it.
    string algo_key = vm["asl.algorithm"].as<string>();
    unique_ptr<Algorithm> algo = get_algorithm(algo_key);
    if (!algo) {
        LOG4CXX_ERROR(logger, "Algorithm " << algo_key << " is unknown. Exiting...");
        return EXIT_FAILURE;
    }
    algo->configure(vm);
    if (vm.count("asl.show_only_route") && vm["asl.show_only_route"].as<bool>()) {
        sg.reset(nullptr);
    }
    return algo->run(sg.get(), rt.get());
}

/**
 * initialize_options creates two options_description objects: the first
 * is for the command line, and the second is for the configuration file.
 */
pair<po::options_description, po::options_description> initialize_options()
{
    // Backend options [tag_backend_options]:
    po::options_description backend("Backend Options");
    Algorithm::options(backend);
    StaticAlgorithm::options(backend);
    ForceDirectedAlgorithm::options(backend);
    LinearFDA::options(backend);
    QuadraticFDA::options(backend);
    ExponentialFDA::options(backend);
    SpringMatchingFDA::options(backend);

    // Frontend options:
    po::options_description frontend("Frontend Options");

#define ALGO_NS "asl."
    frontend.add_options()
        (ALGO_NS "algorithm,"    "A" , po::value<string>()->default_value("base")   , "Labeling/viewing algorithm. Options are: disable, base, static, linear_fd, quadratic_fd, exponential_fd, and spring_matching_fd.")
        (ALGO_NS "show_only_route,o" , po::value<bool>()->default_value(false)      , "Show only streets from the route; that is, hide the rest of the route.")
        ;

#undef ALGO_NS
#define ALGO_NS "test."
    frontend.add_options()
        (ALGO_NS "input_file"       , po::value<string>()                          , "This field takes precedence over straph. Input test data file to be read into street route. For format, see straph library.")
        (ALGO_NS "scale"            , po::value<double>()->default_value(1.0)      , "Scale testdata street route by this value.")
        ;

#undef ALGO_NS
#define ALGO_NS "straph."
    frontend.add_options()
        (ALGO_NS "input_file"     , po::value<string>()                          , "Input shapefile to be read into street graph. It is not necesarry to specify the extension of the file.")
        (ALGO_NS "name_index"     , po::value<unsigned>()->default_value(0)      , "Field index in shapefile '*.dbf' file for street name.")
        (ALGO_NS "normalize"      , po::value<bool>()->default_value(true)       , "Whether to normalize values in street graph (recommended).")
        (ALGO_NS "rename"         , po::value<bool>()->default_value(true)       , "Whether to rename unknown streets in street graph route.")
        (ALGO_NS "scale"          , po::value<double>()->default_value(1.0)      , "Scale street graph by this scalar value.")
        (ALGO_NS "algorithm," "R" , po::value<string>()->default_value("disable"), "Which routing algorithm to use. Options are: disable, dfs, and bfs.")
        (ALGO_NS "print_route,""p", po::value<bool>()->default_value(false)      , "Print the indexes of the major junctions on the route.")
        (ALGO_NS "route,"     "r" , po::value<vector<size_t>>()->multitoken()    , "Starting route path within the map. Provide this option twice, once for each value.")
        (ALGO_NS "require_route," "q", po::value<bool>()->default_value(true)   , "Quit if no route can be found.")
        ;

    // Commandline-only options:
    po::options_description cmdline("Command-line Options");
    cmdline.add_options()
        ("log4cxx,"          "c" , po::value<string>()                          , "log4cxx config file to load")
        ("help,"             "h" ,                                                "print this help message")
        ("version,"          "V" ,                                                "print version string")
        ;

    // Hidden options:
    po::options_description hidden("Hidden Options");
    hidden.add_options()
        ("config"                , po::value<string>(),                           "input configuration")
        ;

    // Commandline options
    po::options_description cmdline_options;
    cmdline_options.add(frontend).add(cmdline).add(hidden);

    // Config file options
    po::options_description config_file_options;
    config_file_options.add(frontend).add(backend);

    return make_pair(cmdline_options, config_file_options);
}

po::variables_map process_options(int argc, char **argv)
{
    int retval = EXIT_FAILURE;

    // Get commandline and config option descriptions
    auto optpair = initialize_options();
    auto cmdline = move(optpair.first);
    auto config = move(optpair.second);

    // Positional options:
    po::positional_options_description pos;
    pos.add("config", 1); // only one instance of config

    // Setup basic logging.
    LOG4CXX_DECODE_CHAR(logstring, "%6r %5p %m%n");
    log4cxx::LayoutPtr def_layout(new log4cxx::PatternLayout(logstring));
    log4cxx::AppenderPtr def_appender(new log4cxx::ConsoleAppender(def_layout));
    log4cxx::BasicConfigurator::configure(def_appender);

    po::variables_map vm (new po::variables_map);
    try {
        // Parse it all
        po::store(po::command_line_parser(argc, argv).options(cmdline).positional(pos).run(), vm);
        po::notify(vm);

        // Read log4cxx configuration file if specified.
        if (vm.count("log4cxx")) {
            auto cfg = vm["log4cxx"].as<string>().c_str();
            log4cxx::PropertyConfigurator::configure(cfg);
        }

        // Read configuration file if specified.
        if (vm.count("config")) {
            auto cfg = vm["config"].as<string>().c_str();
            ifstream ifs(cfg);
            if (!ifs) {
                LOG4CXX_ERROR(logger, "Can not open config file '" << cfg << "'");
                goto usage;
            } else {
                po::store(po::parse_config_file(ifs, config), vm);
                po::notify(vm);
            }
        }
    } catch (exception &e) {
        LOG4CXX_ERROR(logger, "" << e.what());
        goto usage;
    }

    // Maybe we need to exit prematurely because of some user errors?
    // This block checks for possible errors.
    if (vm.count("help")) {
        retval = EXIT_SUCCESS;
        goto usage;
    } else if (vm.count("version")) {
        // TODO: Get the version from CMake
        cout << "tourable version 0.20" << endl;
        exit(EXIT_SUCCESS);
    } else if (vm.count("straph.input_file") == 0 && vm.count("test.input_file") == 0) {
        LOG4CXX_ERROR(logger, "No input file specified!");
        goto usage;
    }

    // Everything good, return our variables map
    return vm;

usage:
    cout << "Usage: " << argv[0] << " [options] CONFIG\n\n"
         << cmdline << "\n"
         << "\nExample:\n"
            "    tourable -c ../log4cxx.conf unterfranken.ini\n"
         << endl;
    exit(retval);
}
