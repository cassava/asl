/*
 * util/augmented_route.cc
 *
 * Ben Morgan <benjamin.morgan@stud-mail.uni-wuerzburg.de>
 * 04. December 2014
 */

#include "augmented_route.h"

#include <straph/point.h>
#include <log4cxx/logger.h>

#include <cmath>
using namespace std;

namespace asl {

namespace {

log4cxx::LoggerPtr logger{log4cxx::Logger::getLogger("asl.router")};

straph::Point route_front(const straph::Route& route)
{
    // Hacky way to get the first point in the route. The first
    // valid point that comes through gets immediately returned.
    for (const straph::RouteSegment& rseg : route) {
        for (const straph::Segment& seg : rseg.segments) {
            for (straph::Point p : seg.polyline) {
                return p;
            }
        }
    }
    return straph::Point();
}

}

AugmentedRoute::AugmentedRoute(const straph::Route& route)
    : route_(route)
    , cur_rseg_id_(0)
    , cur_rseg_(nullptr)
    , cur_seg_id_(0)
    , cur_seg_(nullptr)
    , dest_wp_id_(0)
    , dest_heading_(0.0)
    , cur_heading_(0.0)
    , valid_(true)
    , rotated_(false)
{
    valid_ = reset_rseg();
    if (!valid_) return;
    // If the first point in the route is (0,0), then reset_rseg() has
    // already called next_wp for us, and we don't need to do it.
    if (route_front(route) != straph::Point(0.0, 0.0)) {
        cur_pos_ = dest_wp_;
        valid_ = next_wp();
    }
    cur_heading_ = dest_heading_;
}

double AugmentedRoute::offset() const
{
    double theta = dest_heading_ - cur_heading_;
    if (theta >= M_PI)
        return theta - 2.0*M_PI;
    else if (theta < -M_PI)
        return theta + 2.0*M_PI;
    else
        return theta;
}

std::pair<straph::Point, double> AugmentedRoute::next(double dist, double rot) {
    if (cur_heading_ == dest_heading_) {
        return make_pair(move(dist), cur_heading_);
    } else {
        return make_pair(cur_pos_, rotate(rot));
    }
}

straph::Point AugmentedRoute::move(double dist) {
    rotated_ = false;
    if (!valid_) {
        LOG4CXX_TRACE(logger, "Route is no longer valid!");
        return cur_pos_;
    }

    LOG4CXX_TRACE(logger, "Moving point along route by " << dist << "...");

    if (remaining() <= dist) {
        cur_pos_ = dest_wp_;
        valid_ = next_wp();
        LOG4CXX_DEBUG(logger, "Reached new waypoint " << cur_pos_ << ", next waypoint is " << dest_wp_ << " with an offset of " << offset() << ".");
    } else {
        cur_pos_.translate(dist, cur_heading_);
    }

    return cur_pos_;
}

double AugmentedRoute::rotate(double rot) {
    if (!valid_) {
        LOG4CXX_TRACE(logger, "Route is no longer valid!");
        rotated_ = false;
        return cur_heading_;
    }

    LOG4CXX_TRACE(logger, "Rotating point by " << rot << "...");
    if (abs(offset()) < rot) {
        cur_heading_ = dest_heading_;
    } else {
        double sign = (offset() < 0.0) ? -1.0 : 1.0;
        cur_heading_ += sign*rot;
    }

    rotated_ = true;
    return cur_heading_;
}

bool AugmentedRoute::reset_rseg() {
    if (route_.empty()) {
        return false;
    }
    cur_rseg_id_ = 0;
    cur_rseg_ = &route_[0];
    return reset_seg();
}

bool AugmentedRoute::next_rseg() {
    if (++cur_rseg_id_ >= route_.size()) {
        return false;
    }
    cur_rseg_ = &route_[cur_rseg_id_];
    for (auto func : rseg_callback_) {
        func(route_, cur_rseg_id_);
    }
    return reset_seg();
}

// reset_seg assumes that cur_rseg_ is valid;
bool AugmentedRoute::reset_seg() {
    if (cur_rseg_->segments.empty()) {
        return next_rseg();
    }
    cur_seg_id_ = 0;
    cur_seg_ = &cur_rseg_->segments[0];
    return reset_wp();
}

bool AugmentedRoute::next_seg() {
    if (++cur_seg_id_ >= cur_rseg_->segments.size()) {
        return next_rseg();
    }
    cur_seg_ = &cur_rseg_->segments[cur_seg_id_];
    for (auto func : seg_callback_) {
        func(route_, cur_rseg_id_, cur_seg_id_);
    }
    return reset_wp();
}

bool AugmentedRoute::reset_wp() {
    if (cur_seg_->polyline.empty()) {
        return next_seg();
    }
    dest_wp_id_ = 0;
    straph::Point prev_wp = dest_wp_;
    dest_wp_ = cur_seg_->polyline[0];
    if (prev_wp == dest_wp_) {
        return next_wp();
    }
    dest_heading_ = straph::heading(prev_wp, dest_wp_);
    return true;
}

bool AugmentedRoute::next_wp() {
    if (++dest_wp_id_ >= cur_seg_->polyline.size()) {
        return next_seg();
    }
    straph::Point prev_wp = dest_wp_;
    dest_wp_ = cur_seg_->polyline[dest_wp_id_];
    if (prev_wp == dest_wp_) {
        return next_wp();
    }
    dest_heading_ = straph::heading(prev_wp, dest_wp_);
    for (auto func : wp_callback_) {
        func(route_, cur_rseg_id_, cur_seg_id_, dest_wp_id_);
    }
    return true;
}

} // namespace asl
