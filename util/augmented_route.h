/**
 * \file util/augmented_route.h
 *
 * \author Ben Morgan <benjamin.morgan@stud-mail.uni-wuerzburg.de>
 * \date   12. September 2014
 */

#ifndef INCLUDE_GUARD_127C8C1E42E240AA8304C4CE61BE6227
#define INCLUDE_GUARD_127C8C1E42E240AA8304C4CE61BE6227

#include <straph/straph.h>

#include <list>
#include <utility>

namespace asl {

class AugmentedRoute {
public:
    typedef std::function<void (const straph::Route& r, std::size_t rseg_id)> RouteSegmentCallback;
    typedef std::function<void (const straph::Route& r, std::size_t rseg_id, std::size_t seg_id)> SegmentCallback;
    typedef std::function<void (const straph::Route& r, std::size_t rseg_id, std::size_t seg_id, std::size_t wp_id)> WaypointCallback;

    AugmentedRoute(const straph::Route& route);
    virtual ~AugmentedRoute() {}

    void insert_rseg_callback(RouteSegmentCallback func) { rseg_callback_.push_back(func); }
    void insert_seg_callback(SegmentCallback func) { seg_callback_.push_back(func); }
    void insert_wp_callback(WaypointCallback func) { wp_callback_.push_back(func); }

    bool empty() const { return !valid_; }
    std::string name() const { return cur_rseg_->name; }

    const straph::RouteSegment* segment() const { return cur_rseg_; }
    straph::Point position() const { return cur_pos_; }
    double remaining() const { return cur_pos_.distance(dest_wp_); }

    bool rotated() const { return rotated_; }
    double heading() const { return cur_heading_; }
    double offset() const;

    /**
     * Move the current position by dist and return the new position.
     */
    straph::Point move(double dist);

    /**
     * Rotate the current position by rad and return the new heading.
     */
    double rotate(double rot);

    /**
     * Advance to next logical point, either by moving or by rotating.
     */
    std::pair<straph::Point, double> next(double dist, double rot);

private:
    bool reset_rseg();
    bool next_rseg();
    bool reset_seg();
    bool next_seg();
    bool reset_wp();
    bool next_wp();

private:
    const straph::Route& route_;

    // rseg stands for route segmentment
    std::size_t cur_rseg_id_;
    const straph::RouteSegment* cur_rseg_;

    std::size_t cur_seg_id_;
    const straph::Segment* cur_seg_;

    // wp stands for waypoint.
    std::size_t dest_wp_id_;
    straph::Point dest_wp_;
    double dest_heading_;

    straph::Point cur_pos_;
    double cur_heading_;
    bool valid_;
    bool rotated_;

    std::list<RouteSegmentCallback> rseg_callback_;
    std::list<SegmentCallback> seg_callback_;
    std::list<WaypointCallback> wp_callback_;
};

} // namespace asl
#endif // INCLUDE_GUARD
