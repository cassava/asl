/**
 * \file util/blocker.h
 *
 * This is a class that has been factored out of Voyager in order to make
 * things more understandable. I'm not sure if this is really necessary
 * though. Possibly the whole class is pointless.
 *
 * \author Ben Morgan <benjamin.morgan@stud-mail.uni-wuerzburg.de>
 * \date   10. Septemeber 2014
 */

#ifndef INCLUDE_GUARD_F5103E93DE6A43F391DFE0C0E14F9622
#define INCLUDE_GUARD_F5103E93DE6A43F391DFE0C0E14F9622

#include <boost/thread/mutex.hpp>
#include <boost/thread.hpp>
#include <boost/chrono.hpp>

namespace asl {

class Blocker {
public:
    Blocker() : block_(false) {}

    bool get() {
        return block_;
    }

    void set(bool state) {
        mutex_.lock();
        block_ = state;
        mutex_.unlock();
    }

    void block() {
        mutex_.lock();
        block_ = true;
        mutex_.unlock();
    }

    void unblock() {
        mutex_.lock();
        block_ = false;
        mutex_.unlock();
    }

    bool toggle() {
        mutex_.lock();
        bool retval = block_ = !block_;
        mutex_.unlock();
        return retval;
    }

    void sleep(long ms) {
        block();
        boost::thread th = boost::thread([&] (long ms) {
            boost::this_thread::sleep_for(boost::chrono::milliseconds(ms));
            unblock();
        }, ms);
        th.detach();
    }

private:
    bool         block_;
    boost::mutex mutex_;
};

} // namespace asl
#endif // INCLUDE_GUARD
