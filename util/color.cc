/*
 * util/color.cc
 *
 * Implementation of interface defined in color.h.
 *
 * Ben Morgan <benjamin.morgan@stud-mail.uni-wuerzburg.de>
 * 10. September 2014
 */

#include "color.h"

#include <cstdint>
#include <cstring>
#include <iostream>
#include <map>
using namespace std;

#include <osg/Vec4>

namespace asl {

namespace {

const map<TangoColor, const char*> color_map{
    {Black,       "#000000"},
    {White,       "#ffffff"},
    {Butter1,     "#fce94f"},
    {Butter2,     "#edd400"},
    {Butter3,     "#c4a000"},
    {Orange1,     "#fcaf3e"},
    {Orange2,     "#f57900"},
    {Orange3,     "#ce5c00"},
    {Chocolate1,  "#e9b96e"},
    {Chocolate2,  "#c17d11"},
    {Chocolate3,  "#8f5902"},
    {Chameleon1,  "#73d216"},
    {Chameleon2,  "#4e9a06"},
    {Chameleon3,  "#8ae234"},
    {SkyBlue1,    "#3465a4"},
    {SkyBlue2,    "#204a87"},
    {SkyBlue3,    "#729fcf"},
    {Plum1,       "#ad7fa8"},
    {Plum2,       "#75507b"},
    {Plum3,       "#5c3566"},
    {Scarlet1,    "#ef2929"},
    {Scarlet2,    "#cc0000"},
    {Scarlet3,    "#a40000"},
    {Aluminium1,  "#eeeeec"},
    {Aluminium2,  "#d3d7ef"},
    {Aluminium3,  "#babdb6"},
    {Aluminium4,  "#888a85"},
    {Aluminium5,  "#555753"},
    {Aluminium6,  "#2e3436"}
};

unsigned hex2unsigned(char c)
{
    if (c >= '0' && c <= '9')
        return c - '0';
    else if (c >= 'a' && c <= 'f')
        return 10 + (c - 'a');
    else if (c >= 'A' && c <= 'F')
        return 10 + (c - 'A');
    else
        return 0;
}

float hex2float(const char* c, unsigned length)
{
    double acc (0);
    for (unsigned i = 0; i < length; ++i) {
        acc += hex2unsigned(*(c+i)) * pow(16.0, static_cast<double>(length-i-1));
    }
    return static_cast<float>(acc);
}

/*
 * The parameter buf needs to be at least of size 2.
 * Anything else is undefined behaviour.
 */
void byte2hex(char* buf, uint8_t b)
{
    static const char* vals = "0123456789abcdef";
    buf[0] = vals[b>>4];
    buf[1] = vals[b&0x0f];
}

/*
 * We assume in this function that the double is a value
 * between 0 and 1 and represents a color with 256 values.
 */
void float2hex(char* buf, float d)
{
    static const float max = 255.0; // maximum layer value
    uint8_t b = static_cast<uint8_t>(d*max + 0.5);
    byte2hex(buf, b);
}

} // anonymous namespace

std::string tango2html(TangoColor color)
{
    return color_map.at(color);
}

std::string Color::html() const
{
    char red[2], green[2], blue[2];
    float2hex(red, color_.x());
    float2hex(green, color_.y());
    float2hex(blue, color_.z());
    return string{'#', red[0], red[1], green[0], green[1], blue[0], blue[1], '\0'};
}

osg::Vec4 Color::html2osg_color(const char* hc)
{
    static const float alpha = 1.0f;                        // completely opaque
    static const float max   = 255.0f;                      // maximum layer value (rgb)
    static const float magic = 17.0f/255.0f;                // brings a single hex x to xx value
    static const osg::Vec4 black (0.0f, 0.0f, 0.0f, alpha); // for all other cases

    // String length must be a multiple of 3
    size_t len = strlen(hc);
    if (*hc == '#') {
        --len;
        ++hc;
    }

    // Get the osg color value of hc
    float red, green, blue;
    switch (len) {
        case 3:
            red = hex2float(hc, 1) * magic;
            green = hex2float(hc+1, 1) * magic;
            blue = hex2float(hc+2, 1) * magic;
            return osg::Vec4(red, green, blue, alpha);
        case 6:
            red = hex2float(hc, 2) / max;
            green = hex2float(hc+2, 2) / max;
            blue = hex2float(hc+4, 2) / max;
            return osg::Vec4(red, green, blue, alpha);
        default:
            return black;
    }
}

osg::Vec4 Color::tango2osg_color(TangoColor c)
{
    return html2osg_color(color_map.at(c));
}

} // namespace asl
