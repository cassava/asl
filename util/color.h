/**
 * \file util/color.h
 *
 * OpenSceneGraph allows one to color nodes with a Vec4.
 * This header defines the TangoColor enum and the Color class.
 *
 * \author Ben Morgan <benjamin.morgan@stud-mail.uni-wuerzburg.de>
 * \date   10. September 2014
 */

#ifndef INCLUDE_GUARD_33EA48C89E9B4D358654616FFEE09276
#define INCLUDE_GUARD_33EA48C89E9B4D358654616FFEE09276

#include <string>

#include <osg/Vec4>

namespace asl {

enum TangoColor {
    Black = 0,
    White,
    Butter1,
    Butter2,
    Butter3,
    Orange1,
    Orange2,
    Orange3,
    Chocolate1,
    Chocolate2,
    Chocolate3,
    Chameleon1,
    Chameleon2,
    Chameleon3,
    SkyBlue1,
    SkyBlue2,
    SkyBlue3,
    Plum1,
    Plum2,
    Plum3,
    Scarlet1,
    Scarlet2,
    Scarlet3,
    Aluminium1,
    Aluminium2,
    Aluminium3,
    Aluminium4,
    Aluminium5,
    Aluminium6
};

std::string tango2html(TangoColor color);

class Color {

public:
    Color() : color_(0.0f, 0.0f, 0.0f, 1.0f) {}
    Color(TangoColor color) { set(color); }
    Color(const std::string& color) { set(color); }
    Color(const char *color) { set(color); }
    Color(osg::Vec4 color) { set(color); }
    Color(const Color& color) { set(color); }

    osg::Vec4 vec4() const
    {
        return color_;
    }

    /**
     * Get the HTML representation of the color. If the color
     * has any alpha value, then this is not considered.
     */
    std::string html() const;

    /**
     * Returns the Vec4 associated with the tango color \a c.
     * Throws out_of_range from stdexcept if c is not in TangoColor.
     */
    void set(TangoColor color)
    {
        color_ = tango2osg_color(color);
    }

    /**
     * Same as set(const char *).
     */
    void set(const std::string& color)
    {
        color_ = html2osg_color(color.c_str());
    }

    /**
     * Converts a html hex color code to a Vec4.
     * Accepts the following formats:
     *
     *   - #xxx
     *   - xxx
     *   - #xxxxxx
     *   - xxxxxx
     *
     * If anything else is given, then the fallback color black is returned.
     */
    void set(const char *color)
    {
        color_ = html2osg_color(color);
    }

    void set(osg::Vec4 color)
    {
        color_ = color;
    }

    void set(const Color& color)
    {
        color_ = color.color_;
    }

    // Assignment operators
    Color& operator=(TangoColor color) { set(color); return *this; }
    Color& operator=(const std::string& color) { set(color); return *this; }
    Color& operator=(const char *color) { set(color); return *this; }
    Color& operator=(osg::Vec4 color) { set(color); return *this; }
    Color& operator=(const Color& color) { set(color); return *this; }

private:
    osg::Vec4 tango2osg_color(TangoColor c);
    osg::Vec4 html2osg_color(const char *hc);

private:
    osg::Vec4 color_;
};

} // namespace asl
#endif // INCLUDE_GUARD
