/**
 * \file util/draw.h
 *
 * I am not really very good at graphics programming, so this is the product
 * of not a little fiddling. Here are a few notes and thoughts.
 *
 * Along which axis do we want the streets and the pointer to be. What is
 * the floor in other words, and what is up? I decided that it would be
 * easiest if the floor consists of (x, y), and z is the up direction.
 *
 * The term "normals", which appears in some of the draw_* sources,
 * appears to specify the axis of the geometry objects. I state then
 * something like (0.0f, 0.0f, -1.0f) to say that z is the up-down axis.
 * However, I don't know why I need the minus there.
 *
 * \author Ben Morgan <benjamin.morgan@stud-mail.uni-wuerzburg.de>
 * \date   05. January 2015
 */

#ifndef INCLUDE_GUARD_80EA9851719847C68DE2711F78047F26
#define INCLUDE_GUARD_80EA9851719847C68DE2711F78047F26

#include "color.h"

#include <straph/point.h>
#include <straph/straph.h>

#include <list>
#include <string>

#include <osg/ref_ptr>
#include <osg/Geode>
#include <osg/Geometry>
#include <osg/Group>
#include <osg/PositionAttitudeTransform>
#include <osg/Vec3>
#include <osgText/Font>

namespace asl {

// Forward-declarations
class LabelFactory;

/**
 * A \c Street represents an isolated section of road. The street may wind
 * or turn, but there are no intersections, except at the end or start of the
 * \c Street.
 *
 *
 * StreetFactory creates street geometry objects and street network geodes.
 */
class StreetFactory {
public:
    StreetFactory(double street_width, Color street_color, double route_width, Color route_color);

    static osg::Geometry* createStreet(const straph::Polyline& path, Color color);
    osg::Geode* createStreets(const straph::Straph& graph);
    osg::Geode* createRoute(const straph::Route& route);
    osg::Group* createNetwork(const straph::Straph& graph, const straph::Route& route);

private:
    double street_width_;
    Color street_color_;
    double route_width_;
    Color route_color_;
};

/**
 * How long is the Pointer and what does it look like?
 *
 *              *
 *              *
 *             ***
 *            *   *
 *
 * I guess it looks something like that. It's height is 1.0 ogs,
 * and it's width is 2.0/3.0 ogs.
 *
 * ogs is a made up unit, to represent the size of things in OSG. ;-)
 */
class Pointer : public osg::PositionAttitudeTransform {
public:
    Pointer(double width, const Color& body, const Color& border);

    straph::Point getPoint() const;
    void setPoint(straph::Point pos);

    double getHeading() const;
    void setHeading(double heading);

    double getElevation() const;
    void setElevation(double elevation);

    void setPointAndHeading(straph::Point position, double heading);
};

class Follower : public osg::PositionAttitudeTransform {
public:
    Follower(double lerp) : lerp_(lerp) {}

    void follow(straph::Point pos, double heading);

    straph::Point getPoint() const;
    void setPoint(straph::Point pos);

    double getHeading() const;
    void setHeading(double heading);

    double getElevation() const;
    void setElevation(double elevation);

    void setPointAndHeading(straph::Point position, double heading);

private:
    double lerp_;
    double heading_;
};

/**
 * A Label is a label in a StreetMap.
 */
class Label : public osg::Geode {
friend class LabelFactory;

public:
    std::string getName() const { return text_; }

    straph::Point getPoint() const;

    osg::Vec3 getTextCenterPosition() const;
    osg::Vec3 getTextBasePosition() const;
    osg::Vec3 getLeaderBasePosition() const;

    double getHeight() const;
    void setHeight(double value);
    double adjustHeight(double value);

    void hide();
    void show();
    bool visible() const;

protected:
    /**
     * Only a LabelFactory can instantiate a Label.
     */
    Label(
        osgText::Font*      font,
        double              fontSize,
        const std::string&  text,
        Color               textColor,
        straph::Point       position,
        double              height,
        bool                drawBoundingBoxAndLeader,
        bool                fillBoundingBox,
        double              boundingBoxMargin,
        Color               backgroundColor,
        Color               borderColor
    );

private:
    std::string text_;
    Color border_color_;
};

/**
 * LabelFactory creates Labels. This is the only way to create a Label.
 */
class LabelFactory {
public:
    LabelFactory(
        const std::string&  fontPath,
        double              fontSize,
        Color               textColor,
        double              height,
        bool                drawBoundingBoxAndLeader,
        bool                fillBoundingBox,
        double              boundingBoxMargin,
        Color               backgroundColor,
        Color               borderColor
    );

    Label* create(straph::Point pos, const std::string& text);

private:
    osg::ref_ptr<osgText::Font> font_;
    double font_size_;
    Color text_color_;
    double height_;
    bool draw_box_;
    bool fill_box_;
    double box_margin_;
    Color background_color_;
    Color border_color_;
};

/*
 * toVec3 converts a straph::Point to an osg::Vec3.
 */
inline osg::Vec3 toVec3(const straph::Point& p, double elevation=0.0)
{
    return osg::Vec3(float(p.x), float(p.y), float(elevation));
}

inline osg::Vec3d toVec3d(const straph::Point& p, double elevation=0.0)
{
    return osg::Vec3d(p.x, p.y, elevation);
}

} // namespace asl
#endif // INCLUDE_GUARD
