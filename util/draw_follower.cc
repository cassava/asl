/*
 * util/draw_follower.cc
 *
 * All the code in here just straight copied from draw_pointer.cc :'-(
 * TODO: Clean this code duplication up.
 *
 * Ben Morgan
 * 05. January 2015
 */

#include "draw.h"

#include <cmath>
using namespace std;

using namespace osg;

#include <log4cxx/logger.h>

namespace asl {

namespace {

log4cxx::LoggerPtr logger{log4cxx::Logger::getLogger("asl.draw.follower")};

}

void Follower::follow(straph::Point p, double rot)
{
    straph::Point tp = getPoint();
    tp += (p - tp) * lerp_;
    double tr = getHeading();
    double d = rot - tr;
    if (d >= M_PI) {
        tr += 2.0*M_PI;
        setHeading(tr);
    } else if (d < -M_PI) {
        tr -= 2.0*M_PI;
        setHeading(tr);
    }
    tr += (rot - tr) * lerp_;
    setPointAndHeading(tp, tr);
    LOG4CXX_TRACE(logger, "Follow (" << p << ", " << rot << "): " << tp << ", " << tr);
}

straph::Point Follower::getPoint() const
{
    osg::Vec3 pos = getPosition();
    return straph::Point(pos.x(), pos.y());
}

void Follower::setPoint(straph::Point pos)
{
    double elevation = getElevation();
    setPosition(toVec3d(pos, elevation));
}

double Follower::getHeading() const
{
    return heading_;
}

void Follower::setHeading(double heading)
{
    heading_ = heading;
    static const osg::Vec3 axis(0.0f, 0.0f, -1.0f);
    setAttitude(osg::Quat(heading, axis));
}

double Follower::getElevation() const
{
    return getPosition().z();
}

void Follower::setElevation(double elevation)
{
    straph::Point pos = getPoint();
    setPosition(toVec3d(pos, elevation));
}

void Follower::setPointAndHeading(straph::Point pos, double heading)
{
    setPoint(pos);
    setHeading(heading);
}

} // namespace asl
