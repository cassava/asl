/*
 * util/draw_label.cc
 *
 * Ben Morgan
 * 19. December 2014
 */

#include "draw.h"
#include "color.h"

#include <cassert>
#include <string>
using namespace std;

#include <boost/filesystem.hpp>

#include <osg/Vec3>
#include <osg/LineWidth>
using namespace osg;

#include <osgText/Text>
#include <osgText/Font>

#include <log4cxx/logger.h>

namespace asl {

namespace {

log4cxx::LoggerPtr labelLogger{log4cxx::Logger::getLogger("asl.draw.label")};

Geometry* createLeader(straph::Point pos, double height, Color color)
{
    Geometry* leader = new Geometry();
    Vec3Array* array = new Vec3Array();
    array->push_back(Vec3(pos.x, pos.y, height));
    array->push_back(Vec3(pos.x, pos.y, 0.0f));
    Vec4Array* colors = new Vec4Array(1);
    (*colors)[0] = color.vec4();
    leader->setColorArray(colors);
    leader->setColorBinding(Geometry::BIND_OVERALL);
    leader->setVertexArray(array);
    leader->addPrimitiveSet(new DrawArrays(PrimitiveSet::LINES, 0, 2));
    LineWidth* lineWidth = new osg::LineWidth();
    lineWidth->setWidth(2.0f);
    leader->getOrCreateStateSet()->setAttributeAndModes(lineWidth, osg::StateAttribute::ON);
#ifndef ENABLE_LIGHTING
    // This forces the renderer to always show full color, regardless of perspective.
    leader->getOrCreateStateSet()->setMode(GL_LIGHTING, StateAttribute::OFF);
#endif

    return leader;
}

} // anonymous namespace

LabelFactory::LabelFactory(
    const std::string&  fontPath,
    double              fontSize,
    Color               textColor,
    double              height,
    bool                drawBoundingBoxAndLeader,
    bool                fillBoundingBox,
    double              boundingBoxMargin,
    Color               backgroundColor,
    Color               borderColor
)
    : font_size_(fontSize)
    , text_color_(textColor)
    , height_(height)
    , draw_box_(drawBoundingBoxAndLeader)
    , fill_box_(fillBoundingBox)
    , box_margin_(boundingBoxMargin)
    , background_color_(backgroundColor)
    , border_color_(borderColor)
{
    namespace bfs = boost::filesystem;
    try {
        bfs::path abspath = bfs::canonical(fontPath);
        if (!bfs::exists(abspath)) {
            LOG4CXX_WARN(labelLogger, "Cannot open font file " << abspath.string());
        } else {
            font_ = osgText::readFontFile(fontPath);
        }
    } catch (exception e) {
        LOG4CXX_ERROR(labelLogger, "Having problems getting the absolute path, or the font. :-/");
    }
}

Label* LabelFactory::create(straph::Point pos, const std::string& text)
{
    return new Label(
            font_,
            font_size_,
            text,
            text_color_,
            pos,
            height_,
            draw_box_,
            fill_box_,
            box_margin_,
            background_color_,
            border_color_);
}

Label::Label(
    osgText::Font*      font,
    double              fontSize,
    const std::string&  text,
    Color               textColor,
    straph::Point       position,
    double              height,
    bool                drawBoundingBoxAndLeader,
    bool                fillBoundingBox,
    double              boundingBoxMargin,
    Color               backgroundColor,
    Color               borderColor
)
    : text_(text)
    , border_color_(borderColor)
{
    osgText::Text* txt = new osgText::Text;
    txt->setFont(font);
    txt->setColor(textColor.vec4());
    txt->setCharacterSize(fontSize);
    txt->setText(text);

    // Set display properties and height
    txt->setAlignment(osgText::TextBase::CENTER_BOTTOM);
    txt->setAutoRotateToScreen(true);
    txt->setPosition(toVec3(position, height));

    // Create bounding box and leader if required
    if (drawBoundingBoxAndLeader) {
        typedef osgText::TextBase::DrawModeMask DMM;
        unsigned drawMode = DMM::TEXT | DMM::BOUNDINGBOX;
        if (fillBoundingBox) {
            drawMode |= DMM::FILLEDBOUNDINGBOX;
            txt->setBoundingBoxColor(backgroundColor.vec4());
        } else {
            txt->setBoundingBoxColor(borderColor.vec4());
        }
        txt->setBoundingBoxMargin(boundingBoxMargin);
        txt->setDrawMode(drawMode);
        this->addDrawable(txt);

        Geometry* leader = createLeader(position, height, borderColor);
        this->addDrawable(leader);
    } else {
        this->addDrawable(txt);
    }
}

straph::Point Label::getPoint() const
{
    Vec3 pos = getTextBasePosition();
    return straph::Point(pos.x(), pos.y());
}

osg::Vec3 Label::getLeaderBasePosition() const
{
    Vec3 pos = getTextBasePosition();
    pos.z() = 0.0;
    return pos;
}

osg::Vec3 Label::getTextBasePosition() const
{
    return dynamic_cast<const osgText::Text*>(getDrawable(0))->getPosition();
}

osg::Vec3 Label::getTextCenterPosition() const
{
    return getDrawable(0)->computeBound().center();
}

/*
 * The height that we are getting here if from the street to the base of the text.
 * This we can derive from the setAlignment() function in Label().
 */
double Label::getHeight() const
{
    return dynamic_cast<const osgText::Text*>(this->getDrawable(0))->getPosition().z();
}

/*
 * We are setting the height from the street to the base of the label text.
 */
void Label::setHeight(double value)
{
    assert(value >= 0.0);

    osgText::Text* txt = dynamic_cast<osgText::Text*>(this->getDrawable(0));
    Vec3 pos = txt->getPosition();
    pos.z() = static_cast<float>(value);
    txt->setPosition(pos);

    if (this->getNumDrawables() == 2) {
        this->setDrawable(1, createLeader(straph::Point(pos.x(), pos.y()), value, border_color_));
    }
}

/*
 * This is probably faster than getting and setting in separate function calls.
 */
double Label::adjustHeight(double value)
{
    osgText::Text* txt = dynamic_cast<osgText::Text*>(this->getDrawable(0));
    Vec3 pos = txt->getPosition();

    double height = static_cast<double>(pos.z()) + value;
    if (height < 0.0) {
        height = 0.0;
    }

    pos.z() = static_cast<float>(height);
    txt->setPosition(pos);

    if (this->getNumDrawables() == 2) {
        this->setDrawable(1, createLeader(straph::Point(pos.x(), pos.y()), height, border_color_));
    }

    return height;
}

void Label::hide()
{
    this->setNodeMask(0x0);
}

void Label::show()
{
    this->setNodeMask(~0x0);
}

bool Label::visible() const
{
    NodeMask nm = this->getNodeMask();
    return nm != 0x0;
}

} // namespace asl
