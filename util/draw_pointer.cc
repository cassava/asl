/*
 * util/draw_pointer.cc
 *
 * Some of this code (pointer creation) is directly based on:
 *
 *     Rui Wang, Xuelei Qian. OpenSceneGraph 3 Cookbook.
 *     Packt Publishing (2012). pp. 81-84.
 *
 * Ben Morgan
 * 10. September 2014
 */

#include "draw.h"
#include "color.h"

#include <osg/Array>
#include <osg/Geode>
#include <osg/Geometry>
#include <osg/LineWidth>
#include <osg/ref_ptr>
using namespace osg;

#include <osgUtil/Tessellator>

#include <log4cxx/logger.h>

namespace asl {

namespace {

log4cxx::LoggerPtr pointerLogger{log4cxx::Logger::getLogger("asl.draw.pointer")};

/*
 * Pointer is 1.0 points long and 0.667 wide by default.
 * The width parameter scales that.
 */
osg::ref_ptr<osg::Geode> createPointerGeode(double width, const Color& body_color, const Color& border_color)
{
    LOG4CXX_TRACE(pointerLogger, "Creating Pointer Geode...");

    // Constants in denumerator
    float f0 = 0.0f;
    float f3 = 3.0f;
    // Constants in numerator
    float f1 = 1.0f * width;
    float f2 = 2.0f * width;

    // This is basically a constant; see draw.h header.
    ref_ptr<Vec3Array> normals (new Vec3Array(1));
    (*normals)[0].set( 0.0f, 0.0f, 1.0f );

    // Create vertex array
    ref_ptr<Vec3Array> vertices (new Vec3Array(4));
    (*vertices)[0].set(  f0   ,  f0    , f0 );
    (*vertices)[1].set( -f1/f3, -f1/f3 , f0 );
    (*vertices)[2].set(  f0   ,  f2/f3 , f0 );
    (*vertices)[3].set(  f1/f3, -f1/f3 , f0 );

    ref_ptr<Vec4Array> body_colors (new Vec4Array(1));
    (*body_colors)[0] = body_color.vec4();

    // Build the geometry object
    ref_ptr<Geometry> polygon (new Geometry);
    polygon->setVertexArray( vertices.get() );
    polygon->setColorArray( body_colors.get() );
    polygon->setColorBinding( Geometry::BIND_OVERALL );
    polygon->setNormalArray( normals.get() );
    polygon->setNormalBinding( Geometry::BIND_OVERALL );
    polygon->addPrimitiveSet( new DrawArrays(GL_POLYGON, 0, 4) );

    // Start the tesselation work
    osgUtil::Tessellator tess;
    tess.setTessellationType( osgUtil::Tessellator::TESS_TYPE_GEOMETRY );
    tess.setWindingType( osgUtil::Tessellator::TESS_WINDING_ODD );
    tess.retessellatePolygons( *polygon );

    // Create the border-lines
    ref_ptr<Vec4Array> border_colors (new Vec4Array(1));
    (*border_colors)[0] = border_color.vec4();

    ref_ptr<Geometry> border (new Geometry);
    border->setVertexArray( vertices.get() );
    border->setColorArray( border_colors.get() );
    border->setColorBinding( Geometry::BIND_OVERALL );
    border->addPrimitiveSet(new DrawArrays(GL_LINE_LOOP, 0, 4));
    border->getOrCreateStateSet()->setAttribute(new LineWidth(2.0f));

    // Create Geode object
    ref_ptr<Geode> geode (new Geode);
    geode->addDrawable( polygon.get() );
    geode->addDrawable( border.get() );

#ifndef ENABLE_LIGHTING
    // This forces the renderer to always show full color, regardless of perspective.
    geode->getOrCreateStateSet()->setMode(GL_LIGHTING, StateAttribute::OFF);
#endif

    return geode;
}

} // anonymous namespace

Pointer::Pointer(double width, const Color& body, const Color& border)
{
    ref_ptr<Geode> geode (createPointerGeode(width, body, border));
    this->addChild(geode.get());
}

straph::Point Pointer::getPoint() const
{
    osg::Vec3 pos = getPosition();
    return straph::Point(pos.x(), pos.y());
}

void Pointer::setPoint(straph::Point pos)
{
    double elevation = getElevation();
    setPosition(toVec3d(pos, elevation));
}

double Pointer::getHeading() const
{
    return getAttitude().x();
}

void Pointer::setHeading(double heading)
{
    static const osg::Vec3 axis(0.0f, 0.0f, -1.0f);
    setAttitude(osg::Quat(heading, axis));
}

double Pointer::getElevation() const
{
    return getPosition().z();
}

void Pointer::setElevation(double elevation)
{
    straph::Point pos = getPoint();
    setPosition(toVec3d(pos, elevation));
}

void Pointer::setPointAndHeading(straph::Point pos, double heading)
{
    LOG4CXX_TRACE(pointerLogger, "Setting pointer position to " << pos << " and heading to " << heading << ".");
    setPoint(pos);
    setHeading(heading);
}

} // namespace asl
