/*
 * util/draw_street.cc
 *
 * Ben Morgan <benjamin.morgan@stud-mail.uni-wuerzburg.de>
 * 12. September 2014
 */

#include "draw.h"

#include <straph/straph.h>
using namespace straph;

#include <utility>
#include <vector>
#include <iostream>

#include <osg/ref_ptr>
#include <osg/Array>
#include <osg/LineWidth>
using namespace osg;

#include <log4cxx/logger.h>

namespace asl {

namespace {

log4cxx::LoggerPtr streetLogger{log4cxx::Logger::getLogger("asl.draw.street")};

} // anonymous namespace

StreetFactory::StreetFactory(double street_width, Color street_color, double route_width, Color route_color)
    : street_width_(street_width)
    , street_color_(street_color)
    , route_width_(route_width)
    , route_color_(route_color)
{}

Geometry* StreetFactory::createStreet(const straph::Polyline& path, Color color)
{
    // Set the shape and form of the street:
    Vec3Array* array = new Vec3Array(path.size());
    for (unsigned i = 0; i < path.size(); ++i) {
        (*array)[i] = toVec3(path[i]);
    }

    Geometry* geom = new Geometry();
    geom->setVertexArray(array);

    Vec3Array* normals = new Vec3Array(1);
    (*normals)[0].set(0.0f, 0.0f, 1.0f);
    geom->setNormalArray(normals);
    geom->setNormalBinding(Geometry::BIND_OVERALL);

    Vec4Array* colors = new Vec4Array(1);
    (*colors)[0] = color.vec4();
    geom->setColorArray(colors);
    geom->setColorBinding(Geometry::BIND_OVERALL);

    geom->addPrimitiveSet(new osg::DrawArrays(osg::PrimitiveSet::LINE_STRIP, 0, array->size()));

    return geom;
}

Geode* StreetFactory::createStreets(const straph::Straph& graph)
{
    Geode* geode = new Geode();
    boost::graph_traits<Straph>::edge_iterator ei, ee;
    for (boost::tie(ei, ee) = edges(graph); ei != ee; ++ei) {
        const Segment& s = graph[*ei];
        Geometry* str = createStreet(s.polyline, street_color_);
        geode->addDrawable(str);
    }
    LineWidth* street_width = new LineWidth();
    street_width->setWidth(street_width_);
    geode->getOrCreateStateSet()->setAttributeAndModes(street_width, StateAttribute::ON);
#ifndef ENABLE_LIGHTING
    // This forces the renderer to always show full color, regardless of perspective.
    geode->getOrCreateStateSet()->setMode(GL_LIGHTING, StateAttribute::OFF);
#endif
    return geode;
}

Geode* StreetFactory::createRoute(const straph::Route& route)
{
    Geode* geode = new Geode();
    for (const Polyline& p : straph::get_route_polylines(route)) {
        Geometry* str = createStreet(p, route_color_);
        geode->addDrawable(str);
    }
    LineWidth* route_width = new LineWidth();
    route_width->setWidth(route_width_);
    geode->getOrCreateStateSet()->setAttributeAndModes(route_width, StateAttribute::ON);
#ifndef ENABLE_LIGHTING
    // This forces the renderer to always show full color, regardless of perspective.
    geode->getOrCreateStateSet()->setMode(GL_LIGHTING, StateAttribute::OFF);
#endif
    return geode;
}

Group* StreetFactory::createNetwork(const straph::Straph& graph, const straph::Route& route)
{
    Group* root = new Group();
    root->addChild(createRoute(route));

    // We don't want to insert the same edges from the street graph
    // that are already in the route, because we sometimes get rendering artifacts.
    std::set<Edge> res;
    for (const RouteSegment& rs : route) {
        for (Edge e : rs.edges) {
            res.insert(e);
        }
    }

    Geode* geode = new Geode();
    boost::graph_traits<Straph>::edge_iterator ei, ee;
    for (boost::tie(ei, ee) = edges(graph); ei != ee; ++ei) {
        if (res.count(*ei)) {
            continue;
        }
        const Segment& s = graph[*ei];
        Geometry* str = createStreet(s.polyline, street_color_);
        geode->addDrawable(str);
    }
    LineWidth* street_width = new LineWidth();
    street_width->setWidth(street_width_);
    geode->getOrCreateStateSet()->setAttributeAndModes(street_width, StateAttribute::ON);
#ifndef ENABLE_LIGHTING
    // This forces the renderer to always show full color, regardless of perspective.
    geode->getOrCreateStateSet()->setMode(GL_LIGHTING, StateAttribute::OFF);
#endif

    root->addChild(geode);
    return root;
}

} // namespace asl
