/**
 * \file util/filewriter.h
 *
 * This file defines the \c FileWriter class, which is meant to serve
 * as a drop-in for \c fstream.
 *
 * \author Ben Morgan <benjamin.morgan@stud-mail.uni-wuerzburg.de>
 * \date   04. November 2014
 */

#ifndef INCLUDE_GUARD_2F74205475564FA8892E389AA3A1E3F9
#define INCLUDE_GUARD_2F74205475564FA8892E389AA3A1E3F9

#include <cstdio>
#include <ostream>
#include <sstream>

namespace asl {

/**
 * \c FileWriter serves as a drop-in for \c fstream.
 *
 * This class is used as a work-around a Visual Studio/Open Scene Graph bug
 * which prevents using osgDB in it's entirety if fstream is used elsewhere in
 * the application.
 * Right now it only has the methods that I need.
 * It has not been thoroughly checked, so treat it as experimental code please!
 *
 * This class opens or creates a file in the filesystem as soon as it can,
 * but it writes data only when closing the file.
 * Therefore it is not well suited to writing large files.
 * You have been warned!
 */
class FileWriter {
public:
    FileWriter() : file(nullptr) {}

    FileWriter(const std::string& path, bool truncate=true) {
        open(path, truncate);
    }

    ~FileWriter() {
        if (file != nullptr) {
            close();
        }
    }

    operator std::ostream&() {
        return s;
    }

    /**
     * Check if the \c FileWriter is in a valid state.
     */
    operator bool() const {
        return file != nullptr;
    }

    /**
     * Open the file at the location given by \c path and return whether
     * successful or not.
     *
     * It is possible to open \em again, however no checks are performed to
     * make sure you don't leak a previous open file (maybe this is a copy?)
     * Best thing to do is check yourself with the bool() operator.
     */
    bool open(const std::string& path, bool truncate=true) {
        file = fopen(path.c_str(), truncate ? "w" : "a");
        s.str("");
        s.clear();
        return file != nullptr;
    }

    /**
     * Close the \c FileWriter.
     */
    bool close() {
        if (file == nullptr) {
            return false;
        }

        fputs(s.str().c_str(), file);
        int err = fclose(file);
        file = nullptr;
        return err == 0;
    }

    template <typename T>
    friend std::ostream& operator<<(FileWriter& w, const T& t) {
        return w.s << t;
    }

private:
    FILE* file;
    std::stringstream s;
};

} // namespace asl
#endif // INCLUDE_GUARD
