/*
 * keyboard_event_handler.cc
 *
 * This file is based on a tutorial from:
 * http://www.openscenegraph.org/documentation/NPSTutorials/osgKeyboardInput.htm
 *
 * Ben Morgan <benjamin.morgan@stud-mail.uni-wuerzburg.de>
 * 25. July 2014
 *
 */

#include "keyboard_event_handler.h"

// Std library:
#include <iostream>

// OSG library: requires osgGA
#include <osgGA/GUIEventHandler>
#include <log4cxx/logger.h>

namespace asl {

log4cxx::LoggerPtr kehLogger{log4cxx::Logger::getLogger("asl.keh")};

bool KeyboardEventHandler::addFunction(int whatKey, functionType newFunction)
{
    if (keyFuncMap.end() != keyFuncMap.find(whatKey)) {
        LOG4CXX_WARN(kehLogger, "Duplicate key '" << whatKey << "' ignored.");
        return false;
    } else {
        keyFuncMap[whatKey].keyFunction = newFunction;
        return true;
    }
}

bool KeyboardEventHandler::addFunction(int whatKey, keyStatusType keyPressStatus, functionType newFunction)
{
    if (keyPressStatus == KEY_DOWN) {
        return addFunction(whatKey,newFunction);
    } else { // KEY_UP
        if (keyUPFuncMap.end() != keyUPFuncMap.find(whatKey)) {
            LOG4CXX_WARN(kehLogger, "Duplicate key '" << whatKey << "' ignored.");
            return false;
        } else {
            keyUPFuncMap[whatKey].keyFunction = newFunction;
            return true;
        }
    }
}

bool KeyboardEventHandler::handle(const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& /*aa*/)
{
    bool newKeyDownEvent = false;

    switch (ea.getEventType()) {
    case osgGA::GUIEventAdapter::KEYDOWN:
        {
            keyFunctionMap::iterator itr = keyFuncMap.find(ea.getKey());
            if (itr != keyFuncMap.end()) {
                if ((*itr).second.keyState == KEY_UP) {
                    (*itr).second.keyState = KEY_DOWN;
                    newKeyDownEvent = true;
                }
                if (newKeyDownEvent) {
                    (*itr).second.keyFunction(argdata);
                }
                return true;
            }
            return false;
        }
    case osgGA::GUIEventAdapter::KEYUP:
        {
            keyFunctionMap::iterator itr = keyFuncMap.find(ea.getKey());
            if (itr != keyFuncMap.end()) {
                (*itr).second.keyState = KEY_UP;
            }
            itr = keyUPFuncMap.find(ea.getKey());
            if (itr != keyUPFuncMap.end()) {
                (*itr).second.keyFunction(argdata);
                return true;
            }
            return false;
        }
    default:
        return false;
    }
}

} // namespace asl
