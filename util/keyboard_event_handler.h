/*
 * \file keyboard_event_handler.h
 *
 * This file is based on a tutorial from:
 * http://www.openscenegraph.org/documentation/NPSTutorials/osgKeyboardInput.htm
 *
 * It provides us with a way to listen for keys in an OSG application and
 * perform appropriate actions. Think of it as a binder of function to
 * keypress.
 *
 * In ASL we use it for steering the navigational unit.
 *
 * \author Ben Morgan <benjamin.morgan@stud-mail.uni-wuerzburg.de>
 * \date   30. July 2014
 */

#ifndef INCLUDE_GUARD_B5347658D6BF46E2B5D05C81D7238B9F
#define INCLUDE_GUARD_B5347658D6BF46E2B5D05C81D7238B9F

// OSG library: requires osgGA
#include <osgGA/GUIEventHandler>

namespace asl {

class KeyboardEventHandler : public osgGA::GUIEventHandler
{
public:

    typedef void (*functionType) (void *);
    enum keyStatusType
    {
        KEY_UP, KEY_DOWN
    };

    // A struct for storing current status of each key and
    // function to execute. Keep track of key's state to avoid
    // redundant calls. (If the key is already down, don't call the
    // key down method again.)
    struct functionStatusType
    {
        functionStatusType() {keyState = KEY_UP; keyFunction = NULL;}
        functionType keyFunction;
        keyStatusType keyState;
    };

    // Storage for list of registered key, function to execute and
    // current state of key.
    typedef std::map<int, functionStatusType > keyFunctionMap;

    // Function to associate a key with a function. If the key has not
    // been previously registered, key and function are added to the
    // map of 'key down' events and 'true' is returned. Otherwise, no
    // entry made and false is returned.
    bool addFunction(int whatKey, functionType newFunction);

    // Overloded version allows users to specify if the function should
    // be associated with KEY_UP or KEY_DOWN event.
    bool addFunction(int whatKey, keyStatusType keyPressStatus, functionType newFunction);

    // The handle method checks the current key down event against
    // list of registered key/key status entries. If a match is found
    // and it's a new event (key was not already down) corresponding
    // function is invoked.
    virtual bool handle(const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter&);

    // Overloaded accept method for dealing with event handler visitors
    virtual void accept(osgGA::GUIEventHandlerVisitor& v)
    {
        v.visit(*this);
    }

    void setArgumentData(void *ptr)
    {
        argdata = ptr;
    }

    void *getArgumentData()
    {
        return argdata;
    }

protected:
    // Argument data
    void *argdata;

    // Storage for registered 'key down' methods and key status
    keyFunctionMap keyFuncMap;

    // Storage for registered 'key up' methods and key status
    keyFunctionMap keyUPFuncMap;
};

} // namespace asl

#endif // INCLUDE_GUARD
