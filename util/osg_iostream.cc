/*
 * osg_ostreams.cc
 *
 * Ben Morgan <benjamin.morgan@stud-mail.uni-wuerzburg.de>
 * 08. September 2014
 */

#include "osg_iostream.h"

#include <iostream>

// OSG library: requires osg, osgGA
#include <osg/Vec3>
#include <osg/Quat>
#include <osgGA/NodeTrackerManipulator>

namespace osg {

std::ostream& operator<<(std::ostream& out, const Vec3& v)
{
    return out << "(" << v.x() << ", " << v.y() << ", " << v.z() << ")";
}

std::istream& operator>>(std::istream& in, Vec3& v)
{
    in.ignore(1, '(');
    in >> v.x();
    in.ignore(1, ',');
    in >> v.y();
    in.ignore(1, ',');
    in >> v.z();
    in.ignore(1, ')');
    return in;
}

std::ostream& operator<<(std::ostream& out, const Vec3d& v)
{
    return out << "(" << v.x() << ", " << v.y() << ", " << v.z() << ")";
}

std::istream& operator>>(std::istream& in, Vec3d& v)
{
    in.ignore(1, '(');
    in >> v.x();
    in.ignore(1, ',');
    in >> v.y();
    in.ignore(1, ',');
    in >> v.z();
    in.ignore(1, ')');
    return in;
}

std::ostream& operator<<(std::ostream& out, const Quat& v)
{
    return out << "(" << v.x() << ", " << v.y() << ", " << v.z() << ", " << v.w() << ")";
}

std::ostream& operator<<(std::ostream& out, const Vec4& v)
{
    return out << "(" << v.w() << ", " << v.x() << ", " << v.y() << ", " << v.z() << ")";
}

std::ostream& operator<<(std::ostream& out, const BoundingBox& bb)
{
    return out << "[" << bb.xMin() << ", " << bb.xMax() << "; "
               << bb.yMin() << ", " << bb.yMax() << "; "
               << bb.zMin() << ", " << bb.zMax() << "]";
}

}

namespace osgGA {
using namespace osg;

std::ostream& operator<<(std::ostream& out, const NodeTrackerManipulator& nt)
{
    Vec3d eye, center, up;
    nt.getHomePosition(eye, center, up);
    out << "NodeTrackerManipulator Status:\n"
           "    Heading: " << nt.getHeading() << "\n"
           "    Elevation: " << nt.getElevation() << "\n"
           "    Center: " << nt.getCenter() << "\n"
           "    Rotation: " << nt.getRotation() << "\n"
           "    Distance: " << nt.getDistance() << "\n"
           "    Home Position =>\n"
           "      eye: " << eye << "\n"
           "      center: " << center << "\n"
           "      up: " << up << "\n"
        << std::endl;
    return out;
}

}

