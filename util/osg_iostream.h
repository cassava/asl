/**
 * \file osg_iostream.h
 *
 * This file helps us to display OSG objects better.
 *
 * \author Ben Morgan <benjamin.morgan@stud-mail.uni-wuerzburg.de>
 * \date   08. September 2014
 */

#ifndef INCLUDE_GUARD_32C459AFD5F34603A6841801B46FA5B6
#define INCLUDE_GUARD_32C459AFD5F34603A6841801B46FA5B6

#include <iostream>

// OSG library: requires osg, osgGA
#include <osg/Vec3>
#include <osg/Quat>
#include <osgGA/NodeTrackerManipulator>

namespace osg {

std::ostream& operator<<(std::ostream& out, const Vec3& v);
std::istream& operator>>(std::istream& in, Vec3& v);
std::ostream& operator<<(std::ostream& out, const Vec3d& v);
std::istream& operator>>(std::istream& in, Vec3d& v);

std::ostream& operator<<(std::ostream& out, const Quat& v);
std::ostream& operator<<(std::ostream& out, const Vec4& v);

std::ostream& operator<<(std::ostream& out, const BoundingBox& bb);

}

namespace osgGA {

std::ostream& operator<<(std::ostream& out, const NodeTrackerManipulator& nt);

}

#endif // INCLUDE_GUARD
