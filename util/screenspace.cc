/*
 * util/screenspace.cc
 *
 * Ben Morgan <benjamin.morgan@stud-mail.uni-wuerzburg.de>
 * 19. December 2014
 */

#include "screenspace.h"

#include <algorithm>
using namespace std;

#include <osg/Matrix>
#include <osgText/Text>
using namespace osg;

// WARNING: the following is a hack to count labels behind the
// camera that are technically also 5.0 in front of the camera.
// The reason we have to do it this way maybe has something to
// do with the perspective. At any rate, it's not clean!
#ifndef IGNORE_DISTANCE
#define IGNORE_DISTANCE 10.0
#endif

namespace asl {

double Translator::distance(const osg::Vec3& v) const
{
    Vec3d eye, center, ignore;
    cam_->getViewMatrixAsLookAt(eye, center, ignore);
    return (center-eye)*(v-eye);
}

ScreenSpace Translator::screen() const
{
    const Viewport* vp = cam_->getViewport();
    return ScreenSpace(vp->width(), vp->height());
}

Leader Translator::leader(const Label* label) const
{
    Leader l;
    l.base = point(label->getLeaderBasePosition());
    l.top = point(label->getTextBasePosition());
    l.world = label->getTextBasePosition();
    return l;
}

straph::Point Translator::point(const osg::Vec3& v) const
{
    Vec4 vt(v.x(), v.y(), v.z(), 1.0);
    vt = vt * trans_;
    vt = vt / vt.w();
    return straph::Point(vt.x(), vt.y());
}

Pixel Translator::pixel(const osg::Vec3& v) const
{
    Vec4 vt(v.x(), v.y(), v.z(), 1.0);
    vt = vt * trans_;
    vt = vt / vt.w();
    return Pixel(static_cast<int>(vt.x()), static_cast<int>(vt.y()));
}

ScreenSpace Translator::screenspace(const Label* label) const
{
    // Get left right coordinates
    vector<int> xs; xs.reserve(8);
    vector<int> ys; ys.reserve(8);
    BoundingBox box = label->getDrawable(0)->computeBound();
    for (int i=0; i < 8; i++) {
        Pixel p = pixel(box.corner(i));
        xs.push_back(p.x);
        ys.push_back(p.y);
    };
    int xmin = *min_element(xs.begin(), xs.end());
    int xmax = *max_element(xs.begin(), xs.end());

    // Get up-down coordinates
    int ymin = pixel(label->getTextBasePosition()).y;
    int center = pixel(box.center()).y;
    int ymax = center + (center - ymin);

    ScreenSpace s;
    s.bottom_left_ = Pixel(xmin, ymin);
    s.upper_right_ = Pixel(xmax, ymax);
    s.z_ = distance(label->getBound().center());
    s.behind_ = (s.z_ - IGNORE_DISTANCE) < 0.0;
    return s;
}

} // namespace asl
