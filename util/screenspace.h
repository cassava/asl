/**
 * \file util/screenspace.h
 *
 * An excerpt from “Placing Street Labels in Interactive Navigational Map” by
 * Benjamin Morgan follows.
 *
 * The way annotations are placed in a 3D world can be categorized into world
 * space and screen space. The difference is whether the labels themselves are
 * subject to perspective transformations.  The 3D world is projected through
 * a particular perspective onto the 2D screen. World-space labels are part of
 * the 3D world that is projected and are often embedded directly on the object
 * they label, whereas screen-space labels appear to be placed on the screen
 * after the projection.
 *
 * In ASL, we have labels that are in the world space. But even so, we need to
 * map the world space to the screen space in order to perform calculations
 * such as whether and how much two objects overlap.
 *
 * The following classes are defined in this header:
 *
 * <ul>
 *  <li> Range
 *  <li> Pixel
 *  <li> ScreenSpace
 *  <li> Translator
 * </ul>
 *
 * \author Ben Morgan <benjamin.morgan@stud-mail.uni-wuerzburg.de>
 * \date   19. December 2014
 */

#ifndef INCLUDE_GUARD_A3EC75F8842E4459879DBD6EAB6EEAE5
#define INCLUDE_GUARD_A3EC75F8842E4459879DBD6EAB6EEAE5

#include "draw.h"

#include <ostream>
#include <algorithm>

#include <osg/Camera>
#include <osg/Vec3>

namespace asl {

class Translator;

/**
 * Range represents an interval.
 *
 * This is a simple class, intended to be used for composition.
 * Ignore it for now.
 */
template <typename T>
class Range {
public:
    /**
     * For reasons of efficiency, YOU must make sure that:
     *
     *     start < end
     *
     * If you don't, then this class will not work.
     */
    Range(T start, T end) : a(start), z(end) {}

    /**
     * Determine IF there is an intersection between this and another Ranges r.
     */
    bool intersects(const Range<T>& r)
    {
        return z > r.a && a < r.z;
    }

    /**
     * Determine by HOW MUCH another range overlaps with this one.
     */
    T overlap(const Range<T>& r)
    {
        using namespace std;
        return max(0, min(r.z, z) - max(r.a, a));
    }

    /** Start of interval. */
    T a;

    /** End of interval. */
    T z;
};

/**
 * Pixel represents a pixel on the monitor. It is essentially a point on a 2D grid,
 * with whole numbers. I originally called this ScreenPoint, but it was a long name
 * that looked to similar to ScreenSpace, so I renamed it to Pixel.
 *
 * This is a simple class, intended to be used for composition.
 */
struct Pixel {
    Pixel() : x(0), y(0) {}
    Pixel(int xa, int ya) : x(xa), y(ya) {}

    /// flip swaps the x and y values and returns a new Pixel.
    Pixel flip()
    {
        return Pixel(y, x);
    }

    int ydist(const Pixel& p) { return (y < p.y ? p.y - y : y - p.y); }
    int xdist(const Pixel& p) { return (x < p.x ? p.x - x : x - p.x); }

// Operators:
    Pixel& operator+=(const Pixel& p) { x+=p.x; y+=p.y; return *this; }
    Pixel& operator-=(const Pixel& p) { x-=p.x; y-=p.y; return *this; }
    Pixel& operator*=(double d) { x*=d; y*=d; return *this; }
    Pixel& operator/=(double d) { x/=d; y/=d; return *this; }

    friend Pixel operator+(const Pixel& lhs, const Pixel& rhs) { return Pixel(lhs.x+rhs.x, lhs.y+rhs.y); }
    friend Pixel operator-(const Pixel& lhs, const Pixel& rhs) { return Pixel(lhs.x-rhs.x, lhs.y-rhs.y); }
    friend Pixel operator*(const Pixel& lhs, double rhs) { return Pixel(lhs.x*rhs, lhs.y*rhs); }
    friend Pixel operator/(const Pixel& lhs, double rhs) { return Pixel(lhs.x/rhs, lhs.y/rhs); }
    friend Pixel operator*(double lhs, const Pixel& rhs) { return operator*(rhs, lhs); }

    friend bool operator==(const Pixel& lhs, const Pixel& rhs) { return lhs.x==rhs.x && lhs.y==rhs.y; }
    friend bool operator!=(const Pixel& lhs, const Pixel& rhs) { return !operator==(lhs, rhs); }
    friend bool operator< (const Pixel& lhs, const Pixel& rhs) { return lhs.x<rhs.x || (lhs.x==rhs.x && lhs.y<rhs.y); }
    friend bool operator> (const Pixel& lhs, const Pixel& rhs) { return  operator<(lhs, rhs); }
    friend bool operator<=(const Pixel& lhs, const Pixel& rhs) { return !operator>(lhs, rhs); }
    friend bool operator>=(const Pixel& lhs, const Pixel& rhs) { return !operator<(lhs, rhs); }

    friend std::ostream& operator<<(std::ostream& out, const Pixel& p)
    {
        return out << "(" << p.x << "," << p.y << ")";
    }

// Data Members:
    int x;
    int y;
};

/**
 * ScreenSpace represents the space taken up by a rectangular object in the
 * screen space.
 */
class ScreenSpace {
    friend Translator;

public:
    ScreenSpace()
        : z_(0.0)
        , behind_(false)
    {}

    ScreenSpace(int width, int height)
        : bottom_left_(Pixel(0, 0))
        , upper_right_(width, height)
        , z_(0.0)
        , behind_(false)
    {}

    ScreenSpace(const Pixel& p, const Pixel& q)
        : z_(0.0)
        , behind_(false)
    {
        bottom_left_.x = std::min(p.x, q.x);
        bottom_left_.y = std::min(p.y, q.y);
        upper_right_.x = std::max(p.x, q.x);
        upper_right_.y = std::max(p.y, q.y);
    }

    // Following two functions only make sense for when instantiated by a Translator.
    double z() const { return z_; }
    bool behind() const { return behind_; }

    int xmin() const { return bottom_left_.x; }
    int xmax() const { return upper_right_.x; }
    int ymin() const { return bottom_left_.y; }
    int ymax() const { return upper_right_.y; }

    void margin_top(int d) { upper_right_.y += d; }
    void margin_right(int d) { upper_right_.x += d; }
    void margin_bottom(int d) { bottom_left_.y -= d; }
    void margin_left(int d) { bottom_left_.x -= d; }
    void margin(int top, int right, int bottom, int left)
    {
        margin_top(top);
        margin_right(right);
        margin_bottom(bottom);
        margin_left(left);
    }

    unsigned width() const { return xmax() - xmin(); }
    unsigned height() const { return ymax() - ymin(); }

    unsigned area() const { return width() * height(); }

    Range<int> yrange() const { return Range<int>(ymin(), ymax()); }
    Range<int> xrange() const { return Range<int>(xmin(), xmax()); }

    bool above(const ScreenSpace& s) const { return ymin() > s.ymax(); }
    bool below(const ScreenSpace& s) const { return s.above(*this); }
    bool rightof(const ScreenSpace& s) const { return xmin() > s.xmax(); }
    bool leftof(const ScreenSpace& s) const { return s.rightof(*this); }
    bool closer(const ScreenSpace& s) const { return z_ < s.z_; }

    bool xintersects(const ScreenSpace& s) const { return xmax() > s.xmin() && xmin() < s.xmax(); }
    bool yintersects(const ScreenSpace& s) const { return ymax() > s.ymin() && ymin() < s.ymax(); }
    bool intersects(const ScreenSpace& s) const { return xintersects(s) && yintersects(s); }

    /// inside returns true if this contains s entirely.
    bool contains(const ScreenSpace& s) const
    {
        return xmin() < s.xmin() && s.xmax() < xmax() && ymin() < s.ymin() && s.ymax() < ymax();
    }

    /// xoverlap returns the number of pixels that this and s overlap in the x-axis.
    unsigned xoverlap(const ScreenSpace& s) const
    {
        return std::max(0, std::min(xmax(), s.xmax()) - std::max(xmin(), s.xmin()));
    }

    /// yoverlap returns the number of pixels that this and s overlap in the y-axis.
    unsigned yoverlap(const ScreenSpace& s) const
    {
        return std::max(0, std::min(ymax(), s.ymax()) - std::max(ymin(), s.ymin()));
    }

    /// overlap returns the number of pixels that this and s overlap.
    unsigned overlap(const ScreenSpace& s) const
    {
        int xo = xoverlap(s);
        if (xo == 0) {
            return 0;
        }

        return xo * yoverlap(s);
    }

    /// xoffset returns the maximum x-offset between this and s, and 0 if they overlap.
    unsigned xoffset(const ScreenSpace& s) const
    {
        if (rightof(s)) {
            return xmin() - s.xmax();
        } else if (leftof(s)) {
            return s.xmin() - xmax();
        }
        return 0;
    }

    /// yoffset returns the maximum y-offset between this and s, and 0 if they overlap.
    unsigned yoffset(const ScreenSpace& s) const
    {
        if (above(s)) {
            return ymin() - s.ymax();
        } else if (below(s)) {
            return s.ymin() - ymax();
        }
        return 0;
    }

    /// xsign returns -1.0 if the midpoint of this is left of the midpoint of s, else 1.0.
    double xsign(const ScreenSpace& s) const
    {
        return midpoint() < s.midpoint() ? -1.0 : 1.0;
    }

    /// ysign returns -1.0 if the midpoint of this is below the midpoint of s, else 1.0.
    double ysign(const ScreenSpace& s) const
    {
        return midpoint().flip() < s.midpoint().flip() ? -1.0 : 1.0;
    }

    /// Return the middle point of the box.
    Pixel midpoint() const {
        return Pixel((bottom_left_.x + upper_right_.x)/2, (bottom_left_.y + upper_right_.y)/2);
    }

// Operators:
    friend bool operator==(const ScreenSpace& lhs, const ScreenSpace& rhs)
    {
        return (lhs.bottom_left_ == rhs.bottom_left_)
            && (lhs.upper_right_ == rhs.upper_right_)
            && (lhs.z_ == rhs.z_);
    }
    friend bool operator!=(const ScreenSpace& lhs, const ScreenSpace& rhs) { return !operator==(lhs, rhs); }
    friend bool operator< (const ScreenSpace& lhs, const ScreenSpace& rhs)
    {
        if (lhs.z_ < rhs.z_) {
            return true;
        } else if (lhs.z_ == rhs.z_) {
            Pixel lhp = lhs.midpoint();
            Pixel rhp = rhs.midpoint();
            return lhp.y<rhp.y || (lhp.y==rhp.y && lhp.x<rhp.x);
        }
        return false;
    }
    friend bool operator> (const ScreenSpace& lhs, const ScreenSpace& rhs) { return  operator<(lhs, rhs); }
    friend bool operator<=(const ScreenSpace& lhs, const ScreenSpace& rhs) { return !operator>(lhs, rhs); }
    friend bool operator>=(const ScreenSpace& lhs, const ScreenSpace& rhs) { return !operator<(lhs, rhs); }

    friend std::ostream& operator<<(std::ostream& out, const ScreenSpace& s)
    {
        return out << "{" << s.z_ << ": " << s.bottom_left_ << " / " << s.upper_right_ << "}";
    }

private:
// Data Members:
    Pixel bottom_left_;
    Pixel upper_right_;

    /// z_ represents the distance of the object from the camera.
    double z_;
    /// behind_ is true if the space is behind the camera.
    bool behind_;
};

class Leader {
    friend Translator;
    Leader() {}

public:
    double length() const { return top.distance(base); }

    straph::Point base;
    straph::Point top;
    osg::Vec3 world;
};

/// Translator translates world-space coordinates to screen-space.
class Translator {
public:
    Translator(const osg::Camera* cam) : cam_(cam)
    {
        trans_ = cam->getViewMatrix() * cam->getProjectionMatrix() * cam->getViewport()->computeWindowMatrix();
    }

    ScreenSpace screen() const;
    double distance(const osg::Vec3& v) const;
    Leader leader(const Label* label) const;
    straph::Point point(const osg::Vec3& v) const;
    Pixel pixel(const osg::Vec3& v) const;
    ScreenSpace screenspace(const Label* label) const;

private:
    const osg::Camera* cam_;
    osg::Matrixd trans_;
};

} // namespace asl
#endif // INCLUDE_GUARD
