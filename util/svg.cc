/*
 * util/svg.cc
 *
 * Ben Morgan <benjamin.morgan@stud-mail.uni-wuerzburg.de>
 * 15. Septemeber 2014
 */

#include "svg.h"

#include <string>
#include <iostream>
using namespace std;

#include <boost/filesystem.hpp>

#include <osg/ref_ptr>
#include <osg/Image>
#include <osgDB/WriteFile>
#include <osgViewer/Viewer>

#include <log4cxx/logger.h>

namespace asl {

namespace {

log4cxx::LoggerPtr logger = log4cxx::Logger::getLogger("asl.svg");

} // anonymous namespace

SVGWriter::SVGWriter(const std::string& path, const ScreenSpace& screen)
    : path_(path)
    , screen_(screen)
{
    out_.open(path);
    if (!out_) {
        LOG4CXX_ERROR(logger, "Error opening file " << path << " for writing!");
        return;
    }

    LOG4CXX_DEBUG(logger, "Creating SVG file " << path);
    out_ << "<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"1.1\""
         << " width=\"" << screen.width() << "\" height=\"" << screen.height() << "\">\n";

    // Make the background color white.
    out_ << "<rect width=\"100%\" height=\"100%\" fill=\"white\" />\n";
}

SVGWriter::~SVGWriter()
{
    out_ << "</svg>\n" << flush;
    out_.close();
}

namespace svg {

SVGFunc background(osgViewer::Viewer& viewer)
{
    return [&](std::ostream& os, const ScreenSpace&, const std::string& path) {
        // Unbelievable, that this is the only way to make a screendump of the
        // OpenGL buffer.
        struct ScreendumpCallback : public osg::Camera::DrawCallback {
            ScreendumpCallback(osg::Image* img) : image_(img) {}
            void operator() (osg::RenderInfo &renderInfo) const {
            #ifndef OPENGL_AVAILABLE
                LOG4CXX_DEBUG(logger, "Unable to use glReadBuffer() due to missing library :-/");
            #else
                glReadBuffer(GL_BACK); // TODO: What is this actually for??
            #endif
                osg::GraphicsContext* gc = renderInfo.getState()->getGraphicsContext();
                image_->readPixels(0, 0, gc->getTraits()->width, gc->getTraits()->height, GL_RGBA, GL_UNSIGNED_BYTE);
            }
            osg::Image* image_;
        };

        boost::filesystem::path p = path;
        p.replace_extension(".png");
        LOG4CXX_DEBUG(logger, "Writing snapshot of OpenGL buffer to " << p.string());

        osg::ref_ptr<osg::Image> image = new osg::Image;
        osg::Camera* cam = viewer.getCamera();
        cam->setFinalDrawCallback(new ScreendumpCallback(image));
        viewer.renderingTraversals();
        cam->setFinalDrawCallback(nullptr);

        bool ok = osgDB::writeImageFile(*image, p.string());
        if (!ok) {
            LOG4CXX_WARN(logger, " -> unable to write snapshot!");
            return;
        }

        os << "<image width=\"100%\" height=\"100%\" xlink:href=" << p.filename() << " />\n";
    };
}

SVGFunc text(int x, int y, const std::string& str) {
    return [=](std::ostream& os, const ScreenSpace& screen, const std::string&) {
        os << "\t<text x=\"" << x << "\" y=\"" << screen.height() - y << "\" font-family=\"Verdana\" font-size=\"20\">"
           << str << "</text>\n";
    };
}

unsigned long Rect::ids_ = 0;

void Rect::svg(std::ostream& os, const ScreenSpace& screen, const std::string&) const
{
    os << "\t<rect x=\"" << s_.xmin() << "\" y=\"" << screen.height() - s_.ymin() - s_.height() << "\""
       << " width=\"" << s_.width() << "\" height=\"" << s_.height() << "\""
       << " fill=\"" << fill_color_ << "\" fill-opacity=\"" << fill_opacity_ << "\""
       << " stroke=\"" << stroke_color_ << "\" stroke-width=\"" << stroke_width_ << "\" />\n";
}

void HatchedRect::svg(std::ostream& os, const ScreenSpace& screen, const std::string&) const
{
    os << "\t<pattern id=\"diagonalHatch" << id_ << "\" width=\"" << hatch_padding_ << "\" height=\"" << hatch_padding_ << "\""
          << " patternTransform=\"rotate(" << hatch_angle_ << " 0 0)\" patternUnits=\"userSpaceOnUse\">\n"
       << "\t\t<line x1=\"0\" y1=\"0\" x2=\"0\" y2=\""<< hatch_padding_ << "\""
          << " style=\"stroke: " << hatch_stroke_color_ << "; stroke-width: " << hatch_stroke_width_ << "\" />\n"
       << "\t</pattern>\n";
    os << "\t<rect x=\"" << s_.xmin() << "\" y=\"" << screen.height() - s_.ymin() - s_.height()<< "\""
       << " width=\"" << s_.width() << "\" height=\"" << s_.height() << "\""
       << " fill=\"url(#diagonalHatch" << id_ << ")\" />\n";
}

void FlyingRect::svg(std::ostream& os, const ScreenSpace& screen, const std::string& path) const
{
    unsigned x = s_.xmin() + 5;
    unsigned y = s_.ymin();
    unsigned width = s_.width() - 5;
    unsigned height = s_.height();

    // TODO: I'm not sure that that this is actually correct. Need to think this through.
    // Normally, it should be correct, but I reverse my logic with ! because we flip the
    // rectangle when we do screen.height() - y - height.
    if (!flyer_below_) {
        y--;
        y -= height/5;
    } else {
        y++;
        y += height;
    }
    height /= 5;

    os << "\t<pattern id=\"flying" << id_ << "\" width=\"10\" height=\"10\" patternUnits=\"userSpaceOnUse\">\n"
          "\t\t<line x1=\"0\" y1=\"0\" x2=\"0\" y2=\"10\" style=\"stroke: " << flyer_stroke_color_
          << "; stroke-width: " << flyer_stroke_width_ <<  "\" />\n"
          "\t</pattern>\n";
    os << "\t<rect x=\"" << x << "\" y=\"" << screen.height() - y - height<< "\""
       << " width=\"" << width << "\" height=\"" << height << "\""
       << " fill=\"url(#flying" << id_ << ")\" />\n";

    // Draw the rectangle itself now
    Rect::svg(os, screen, path);
}

} // namespace svg
} // namespace asl
