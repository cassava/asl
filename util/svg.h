/**
 * \file util/svg.h
 *
 * Here we provide functions to create SVG images of various OSG objects; in
 * particular however, we are interested in creating a descriptive image of the
 * frames during force-directed labeling.
 *
 * \author Ben Morgan <benjamin.morgan@stud-mail.uni-wuerzburg.de>
 * \date   20. Septemeber 2014
 */

#ifndef INCLUDE_GUARD_D315926F3E984CDC934C70B0FFC41BA3
#define INCLUDE_GUARD_D315926F3E984CDC934C70B0FFC41BA3

#include "screenspace.h"
#include "filewriter.h"

#include <cassert>
#include <string>
#include <functional>

namespace osgViewer { class Viewer; }

namespace asl {

/// SVGFunc is a function signature for SVGWriter.
typedef std::function<void(std::ostream&, const ScreenSpace&, const std::string&)> SVGFunc;

/// SVG is an interface to be implemented by all classes that want to be drawn
/// via SVGWriter.
class SVG {
public:
    virtual ~SVG() {}

    /// svg is the function signature that needs to be implemented to satisfy the
    /// interface. The variable screen is a box the size of the SVG image, and path
    /// is the path to which it will be written.
    ///
    /// So far, the path is only needed if you are going to write other images or files
    /// to include in the SVG, such as the svg::background function does.
    virtual void svg(std::ostream& os, const ScreenSpace& screen, const std::string& path) const = 0;
};

class SVGWriter {
public:
    SVGWriter(const std::string& path, const ScreenSpace& screen);
    ~SVGWriter();

    friend SVGWriter& operator<<(SVGWriter& w, SVGFunc f) { f(w.out_, w.screen_, w.path_); return w; }
    friend SVGWriter& operator<<(SVGWriter& w, const SVG& s) { s.svg(w.out_, w.screen_, w.path_); return w; }

private:
    std::string   path_;
    FileWriter    out_;
    ScreenSpace   screen_;
};

namespace svg {

SVGFunc text(int x, int y, const std::string& str);
SVGFunc background(osgViewer::Viewer& viewer);

class Rect : public SVG {
public:
    Rect(const ScreenSpace& s)
        : fill_color_("#babdb6")
        , fill_opacity_(0.8)
        , stroke_color_("black")
        , stroke_width_(2.0)
        , s_(s) {
        id_ = ++ids_;
    }

    Rect(int x, int y, unsigned width, unsigned height)
        : fill_color_("#babdb6")
        , fill_opacity_(0.8)
        , stroke_color_("black")
        , stroke_width_(2.0) {
        id_ = ++ids_;
        s_ = ScreenSpace(Pixel(x, y), Pixel(x+width, y+height));
    }

    virtual ~Rect() {}

    virtual void svg(std::ostream& os, const ScreenSpace& screen, const std::string&) const;

    void setFillColor(const std::string& c) { fill_color_ = c; }
    void setFillOpacity(double o) { fill_opacity_ = o; }
    void setStrokeColor(const std::string& c) { stroke_color_ = c; }
    void setStrokeWidth(double w) { stroke_width_ = w; }

protected:
    std::string fill_color_;
    double fill_opacity_;
    std::string stroke_color_;
    double stroke_width_;
    unsigned long id_;
    ScreenSpace s_;

private:
    static unsigned long ids_;
};

class HatchedRect : public Rect {
public:
    HatchedRect(const ScreenSpace& s)
        : Rect(s)
        , hatch_angle_(0.0)
        , hatch_padding_(10)
        , hatch_stroke_color_("black")
        , hatch_stroke_width_(2.0) {}

    void svg(std::ostream& os, const ScreenSpace& screen, const std::string&) const;

    void setHatchAngle(double a) {
        assert((a > 0.0 && a < 90.0) || (a > 90.0 && a < 180.0));
        hatch_angle_ = a;
    }
    void setHatchPadding(unsigned p) { hatch_padding_ = p; }
    void setHatchStrokeColor(const std::string& c) { hatch_stroke_color_ = c; }
    void setHatchStrokeWidth(double w) { hatch_stroke_width_ = w; }

protected:
    double hatch_angle_;
    unsigned hatch_padding_;
    std::string hatch_stroke_color_;
    double hatch_stroke_width_;
};

class FlyingRect : public Rect {
public:
    FlyingRect(const ScreenSpace& s)
        : Rect(s)
        , flyer_below_(true)
        , flyer_stroke_color_("#555753")
        , flyer_stroke_width_(1.0) {}

    void svg(std::ostream& os, const ScreenSpace& screen, const std::string&) const;

    void setFlyerBelow(bool b) { flyer_below_ = b; }
    void setFlyerStrokeColor(const std::string& c) { flyer_stroke_color_ = c; }
    void setFlyerStrokeWidth(double w) { flyer_stroke_width_ = w; }

protected:
    bool flyer_below_;
    std::string flyer_stroke_color_;
    double flyer_stroke_width_;
};

} // namespace svg

} // namespace asl
#endif // INCLUDE_GUARD
