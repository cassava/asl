/**
 * \file util/timer.h
 *
 * \author Ben Morgan <benjamin.morgan@stud-mail.uni-wuerzburg.de>
 * \date   13. October 2014
 */

#ifndef INCLUDE_GUARD_CD4ECB538D074A1EB1E3624D9AAAE5AF
#define INCLUDE_GUARD_CD4ECB538D074A1EB1E3624D9AAAE5AF

#include <boost/date_time/posix_time/posix_time.hpp>

namespace asl {

class Timer {
public:
    Timer(boost::posix_time::time_duration* dur_ptr=nullptr, bool dur_add=false) {
        running_ = true;
        start_time_ = boost::posix_time::microsec_clock::local_time();
        duration_ptr_ = dur_ptr;
        add_duration_ = dur_add;
    }

    ~Timer() {
        if (running_ && duration_ptr_ != nullptr) {
            if (add_duration_) {
                *duration_ptr_ += get();
            } else {
                *duration_ptr_ = get();
            }
        }
    }

    void reset() {
        running_ = true;
        start_time_ = boost::posix_time::microsec_clock::local_time();
    }

    boost::posix_time::time_duration stop() {
        boost::posix_time::time_duration dur = get();
        running_ = false;
        return dur;
    }

    boost::posix_time::time_duration get() {
        if (running_) {
            stop_time_ = boost::posix_time::microsec_clock::local_time();
        }
        return stop_time_ - start_time_;
    }

private:
    bool running_;
    bool add_duration_;
    boost::posix_time::ptime start_time_;
    boost::posix_time::ptime stop_time_;
    boost::posix_time::time_duration* duration_ptr_;
};

} // namespace asl
#endif // INCLUDE_GUARD
