/*
 * voyager.cc
 *
 * Ben Morgan <benjamin.morgan@stud-mail.uni-wuerzburg.de>
 * 05. January 2015
 */

#include "voyager.h"
#include "util/keyboard_event_handler.h"

#include <straph/point.h>

#include <cassert>
#include <string>
#include <utility>
using namespace std;

#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>

#include <log4cxx/logger.h>

// ALGO_NS is the configuration namespace for this class.
#define ALGO_NS "voyager."

namespace asl {

namespace {

log4cxx::LoggerPtr logger = log4cxx::Logger::getLogger("asl.voyager");
log4cxx::LoggerPtr pclog  = log4cxx::Logger::getLogger("asl.voyager.controller");

const double percentChange = 0.25;

void increaseSpeed(void *arg)
{
    Voyager* p = reinterpret_cast<Voyager*>(arg);

    double speed = p->getSpeed();
    p->setSpeed(speed + (speed * percentChange));
    LOG4CXX_DEBUG(pclog, "Increasing voyager speed to " << p->getSpeed() << ".");
}

void decreaseSpeed(void *arg)
{
    Voyager* p = reinterpret_cast<Voyager*>(arg);

    double speed = p->getSpeed();
    p->setSpeed(speed - (speed * percentChange));
    LOG4CXX_DEBUG(pclog, "Decreasing voyager speed to " << p->getSpeed() << ".");
}

void increaseRotation(void *arg)
{
    Voyager* p = reinterpret_cast<Voyager*>(arg);

    double speed = p->getCornerSpeed();
    p->setCornerSpeed(speed + (speed * percentChange));
    LOG4CXX_DEBUG(pclog, "Increasing voyager corner rotation speed to " << p->getCornerSpeed() << ".");
}

void decreaseRotation(void *arg)
{
    Voyager* p = reinterpret_cast<Voyager*>(arg);

    double speed = p->getCornerSpeed();
    p->setCornerSpeed(speed - (speed * percentChange));
    LOG4CXX_DEBUG(pclog, "Decreasing voyager corner rotation speed to " << p->getCornerSpeed() << ".");
}

void pause(void *arg)
{
    reinterpret_cast<Voyager*>(arg)->blocker.block();
    LOG4CXX_DEBUG(pclog, "Pausing voyager movement.");
}

void toggle(void *arg)
{
    auto blocked = reinterpret_cast<Voyager*>(arg)->blocker.toggle();
    auto text = blocked ? "stop" : "continue";
    LOG4CXX_DEBUG(pclog, "Toggling voyager to " << text << " movement.");
}

void play(void *arg)
{
    reinterpret_cast<Voyager*>(arg)->blocker.unblock();
    LOG4CXX_DEBUG(pclog, "Starting voyager movement.");
}

} // anonymous namespace

void Voyager::options(boost::program_options::options_description& opt)
{
    namespace po = boost::program_options;

    opt.add_options()
        (ALGO_NS "travel_speed" , po::value<double>()->default_value(1/60.0), "Travel speed, in osg units, of pointer on streets straights.")
        (ALGO_NS "corner_speed" , po::value<double>()->default_value(1/30.0), "Rotation speed, in radians, of pointer when cornering.")
        (ALGO_NS "start_stopped", po::value<bool>()->default_value(true)    , "Whether upon initialization, the pointer should wait for user input instead of moving right away.")
        (ALGO_NS "camera_lerp"  , po::value<double>()->default_value(0.05)  , "The level of linear interpolation to be applied to pointer movement.")
        ;
}

void Voyager::configure(const boost::program_options::variables_map& vm)
{
    travel_speed_  = vm[ALGO_NS "travel_speed"].as<double>();
    corner_speed_  = vm[ALGO_NS "corner_speed"].as<double>();
    start_stopped_ = vm[ALGO_NS "start_stopped"].as<bool>();
    lerp_          = vm[ALGO_NS "camera_lerp"].as<double>();
    blocker.set(start_stopped_);

    if (lerp_ <= 0.0 || lerp_ > 1.0) {
        LOG4CXX_WARN(logger, "Option camera_lerp should be in range (0,1], but is " << lerp_);
        LOG4CXX_WARN(logger, "Disabling linear interpolation of pointer movement.");
        lerp_ = 1.0;
    }
}

bool Voyager::initialize(const straph::Route& route, Pointer* pointer)
{
    assert(pointer != nullptr);

    pointer_ = pointer; // automatic conversion to osg::ref_ptr
    follower_ = new Follower(lerp_);
    route_.reset(new AugmentedRoute(route));
    if (route_->empty()) {
        return false;
    }

    // Set pointer initial position and heading
    straph::Point pos = route_->position();
    double heading = route_->heading();
    pointer_->setPointAndHeading(pos, heading);
    follower_->setPointAndHeading(pos, heading);
    return true;
}

bool Voyager::update()
{
    if (blocker.get()) {
        follower_->follow(route_->position(), route_->heading());
        return !route_->empty();
    }

    pair<straph::Point, double> p = route_->next(travel_speed_, corner_speed_);
    pointer_->setPointAndHeading(p.first, p.second);
    follower_->follow(p.first, p.second);
    return !route_->empty();
}

KeyboardEventHandler* createVoyagerController(Voyager* v)
{
    KeyboardEventHandler *keh (new KeyboardEventHandler);
    keh->setArgumentData(v);

    // Increase/decrease speed functions
    keh->addFunction('a', increaseSpeed);
    keh->addFunction('z', decreaseSpeed);

    // Increase/decrease roatation functions
    keh->addFunction('A', increaseRotation);
    keh->addFunction('Z', decreaseRotation);

    // Movement (left/right/up/down) functions
    keh->addFunction('c', KeyboardEventHandler::KEY_DOWN, pause);
    keh->addFunction('v', KeyboardEventHandler::KEY_DOWN, toggle);
    keh->addFunction('b', KeyboardEventHandler::KEY_DOWN, play);

    return keh;
}

} // namespace asl
