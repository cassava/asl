/**
 * \file voyager.h
 *
 * \author Ben Morgan <benjamin.morgan@stud-mail.uni-wuerzburg.de>
 * \date   05. January 2015
 */

#ifndef INCLUDE_GUARD_32C56516B2E44850899DF509ADF615AC
#define INCLUDE_GUARD_32C56516B2E44850899DF509ADF615AC

#include "util/augmented_route.h"
#include "util/blocker.h"
#include "util/draw.h"
#include "util/keyboard_event_handler.h"

#include <straph/routing.h>

#include <list>

#include <osg/ref_ptr>

#include <log4cxx/logger.h>

// Forward declaration optimization
namespace boost {
    namespace program_options {
        // #include <boost/program_options/options_description.hpp>
        class options_description;
        // #include <boost/program_options/variables_map.hpp>
        class variables_map;
    }
}

namespace asl {

class Voyager {
public:
    /**
     * Create a Voyager.
     */
    Voyager() {}

    /**
     * You can pause, toggle, sleep the movement of this pointer by using
     * this blocker.
     */
    Blocker blocker;

    /**
     * Write options into boost program options. Needs to only be done once.
     */
    static void options(boost::program_options::options_description& opt);

    /**
     * Read options from boost program options variable map.
     * Needs to be done for each Voyager.
     */
    void configure(const boost::program_options::variables_map& config);

    /**
     * Call when you have a route.
     */
    bool initialize(const straph::Route& route, Pointer* pointer);

    /**
     * To be called before (or after) each frame is displayed.
     * Returns true as long as the route is not empty (ie. valid.)
     */
    bool update();

public:
    /** Get the pointer node, which can then be added to the root node. */
    Pointer* getPointer() const { return pointer_.get(); }

    /** Get the pseudo pointer, which is to be tracked. */
    Follower* getFollower() const { return follower_.get(); }

    /** Get the current route; pointer will become invalid when this class is gone. */
    const AugmentedRoute* getRoute() const { return route_.get(); }

    /** Set the speed of the animation. */
    void setSpeed(double speed) { travel_speed_ = speed; }

    /** Get the current speed of the animation. */
    double getSpeed() const { return travel_speed_; }

    /** Set the speed that corners are taken (the rotation speed). */
    void setCornerSpeed(double speed) { corner_speed_ = speed; }

    /** Get the current corner speed. */
    double getCornerSpeed() const { return corner_speed_; }

    /** Return whether the last movement was a rotation. */
    bool didRotate() const { return route_->rotated(); }

private:
    std::unique_ptr<AugmentedRoute> route_;
    osg::ref_ptr<Pointer> pointer_;
    osg::ref_ptr<Follower> follower_;

    double travel_speed_;
    double corner_speed_;
    bool   start_stopped_;
    double lerp_;
};

/**
 * Factory for creating a KeyboardEventHandler that can
 * manipulate the voyager.
 *
 * See the source file for the key bindings.
 */
KeyboardEventHandler* createVoyagerController(Voyager* v);

} // namespace asl
#endif // INCLUDE_GUARD
